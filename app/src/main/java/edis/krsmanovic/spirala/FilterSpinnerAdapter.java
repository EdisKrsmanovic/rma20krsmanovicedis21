package edis.krsmanovic.spirala;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import edis.krsmanovic.spirala.TransactionList.TransactionType;


public class FilterSpinnerAdapter extends BaseAdapter {

    private Context context;
    private LayoutInflater layoutInflater;
    private List<String> filterList;
    private List<TransactionType> transactionTypes = new ArrayList<>();
    private int position;
    private int img[] = {0,R.drawable.ic_individualpayment, R.drawable.ic_regularpayment, R.drawable.ic_purchase, R.drawable.ic_individualincome, R.drawable.ic_regularincome, 0};

    public FilterSpinnerAdapter(Context context, List<String> filterList) {
        this.context = context;
        this.filterList = filterList;
        layoutInflater = (LayoutInflater.from(context));
        transactionTypes.add(null);
        transactionTypes.add(TransactionType.INDIVIDUALPAYMENT);
        transactionTypes.add(TransactionType.REGULARPAYMENT);
        transactionTypes.add(TransactionType.PURCHASE);
        transactionTypes.add(TransactionType.INDIVIDUALINCOME);
        transactionTypes.add(TransactionType.REGULARINCOME);
        if(filterList.size() == 6) {
            img = new int[]{R.drawable.ic_individualpayment, R.drawable.ic_regularpayment, R.drawable.ic_purchase, R.drawable.ic_individualincome, R.drawable.ic_regularincome, 0};
        }
    }

    @Override
    public int getCount() {
        return filterList.size()-1;
    }

    @Override
    public TransactionType getItem(int position) {
        if(position == 0 || position == 6) return null;
        return transactionTypes.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        this.position = position;
        View rowView = layoutInflater.inflate(R.layout.filter_spinner_item, null);

        TextView transactionText = (TextView) rowView.findViewById(R.id.filterText);
        transactionText.setText(filterList.get(position));

        ImageView transactionIcon = (ImageView) rowView.findViewById(R.id.filterIcon);
        transactionIcon.setImageResource(img[position]);
        return rowView;
    }
}