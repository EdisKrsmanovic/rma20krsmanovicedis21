package edis.krsmanovic.spirala;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class MyContentProvider extends ContentProvider {

    private static final int ALLTRANSACTIONROWS = 1;
    private static final int ALLACCOUNTROWS = 2;
    private static final int TODELETEROWS = 3;
    private static final UriMatcher uM;

    static {
        uM = new UriMatcher(UriMatcher.NO_MATCH);
        uM.addURI("rma.my.provider", "transactions", ALLTRANSACTIONROWS);
        uM.addURI("rma.my.provider", "account", ALLACCOUNTROWS);
        uM.addURI("rma.my.provider", "toDelete", TODELETEROWS);
        //content://rma.my.provider/toDelete

    }

    MyDBOpenHelper mHelper;

    @Override
    public boolean onCreate() {
        mHelper = new MyDBOpenHelper(getContext(),
                MyDBOpenHelper.DATABASE_NAME, null,
                MyDBOpenHelper.DATABASE_VERSION);
        return false;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder) {
        SQLiteDatabase database;
        try {
            database = mHelper.getWritableDatabase();
        } catch (SQLiteException e) {
            database = mHelper.getReadableDatabase();
        }
        String groupBy = null;
        String having = null;
        SQLiteQueryBuilder squery = new SQLiteQueryBuilder();
        if (uri.toString().contains("transactions")) {
            squery.setTables(MyDBOpenHelper.TRANSACTION_TABLE);
        } else if (uri.toString().contains("account")) {
            squery.setTables(MyDBOpenHelper.ACCOUNT_TABLE);
        } else if (uri.toString().contains("toDelete")) {
            squery.setTables(MyDBOpenHelper.TODELETE_TABLE);
        }
        return squery.query(database, projection, selection, selectionArgs, groupBy, having, sortOrder);
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        switch (uM.match(uri)) {
            case ALLTRANSACTIONROWS:
            case ALLACCOUNTROWS:
                return "vnd.android.cursor.dir/vnd.rma.elemental";
            default:
                throw new IllegalArgumentException(
                        "Unsupported uri: " + uri.toString());
        }
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues values) {
        SQLiteDatabase db = mHelper.getWritableDatabase();
        if (uri.toString().contains("transactions")) {
            db.insert(MyDBOpenHelper.TRANSACTION_TABLE, null, values);
        } else if (uri.toString().contains("account")) {
            db.insert(MyDBOpenHelper.ACCOUNT_TABLE, null, values);
        } else if (uri.toString().contains("toDelete")) {
            db.insert(MyDBOpenHelper.TODELETE_TABLE, null, values);
        }
        return null;
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
        SQLiteDatabase db = mHelper.getWritableDatabase();
        if (uri.toString().contains("transactions")) {
            db.delete(MyDBOpenHelper.TRANSACTION_TABLE, null, null);
        } else if (uri.toString().contains("account")) {
            db.delete(MyDBOpenHelper.ACCOUNT_TABLE, null, null);
        } else if (uri.toString().contains("toDelete")) {
            db.delete(MyDBOpenHelper.TODELETE_TABLE, null, null);
        }
        return 0;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {
        return 0;
    }
}
