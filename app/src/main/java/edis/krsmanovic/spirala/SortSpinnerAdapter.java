package edis.krsmanovic.spirala;

import android.content.Context;
import android.widget.ArrayAdapter;

import java.util.List;

public class SortSpinnerAdapter extends ArrayAdapter<String> {

    public SortSpinnerAdapter(Context theContext, List<String> objects, int theLayoutResId) {
        super(theContext, theLayoutResId, objects);
    }

    @Override
    public int getCount() {
        // don't display last item. It is used as hint.
        int count = super.getCount();
        return count;
    }
}