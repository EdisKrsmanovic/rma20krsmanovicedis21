package edis.krsmanovic.spirala.Budget;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.fragment.app.Fragment;

import com.google.android.material.textfield.TextInputEditText;

import edis.krsmanovic.spirala.HttpService;
import edis.krsmanovic.spirala.MainActivity;
import edis.krsmanovic.spirala.MyDBOpenHelper;
import edis.krsmanovic.spirala.R;
import edis.krsmanovic.spirala.ScreenSlidePagerAdapter;

public class BudgetFragment extends Fragment {

    private ScreenSlidePagerAdapter screenSlidePagerAdapter;

    TextInputEditText budgetText;
    TextInputEditText totalLimitText;
    TextInputEditText monthLimitText;
    Button saveButton;

    public BudgetFragment(ScreenSlidePagerAdapter screenSlidePagerAdapter) {
        this.screenSlidePagerAdapter = screenSlidePagerAdapter;
    }

    public BudgetFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.budget_detail, container, false);
        budgetText = (TextInputEditText) view.findViewById(R.id.budgetText);
        totalLimitText = (TextInputEditText) view.findViewById(R.id.totalLimitText);
        monthLimitText = (TextInputEditText) view.findViewById(R.id.monthLimitText);
        saveButton = (Button) view.findViewById(R.id.saveButton);

        budgetText.setText(MainActivity.account.getBudget().toString());
        totalLimitText.setText(MainActivity.account.getTotalLimit().toString());
        monthLimitText.setText(MainActivity.account.getMonthLimit().toString());
        saveButton.setOnClickListener(e -> save());
        return view;
    }

    private void save() {
        MainActivity.account.setBudget(Double.parseDouble(budgetText.getText().toString()));
        MainActivity.account.setTotalLimit(Double.parseDouble(totalLimitText.getText().toString()));
        MainActivity.account.setMonthLimit(Double.parseDouble(monthLimitText.getText().toString()));
        screenSlidePagerAdapter.notifyDataSetChanged();
        if(!MainActivity.hasInternetAccess) {
            ContentResolver cr = getContext().getApplicationContext().getContentResolver();
            Uri adresa = Uri.parse("content://rma.my.provider/account");

            ContentValues contentValue = new ContentValues();
            contentValue.put(MyDBOpenHelper.ACCOUNT_BUDGET, Double.parseDouble(budgetText.getText().toString()));
            contentValue.put(MyDBOpenHelper.ACCOUNT_MONTHLIMIT, Double.parseDouble(monthLimitText.getText().toString()));
            contentValue.put(MyDBOpenHelper.ACCOUNT_TOTALLIMIT, Double.parseDouble(totalLimitText.getText().toString()));
            cr.insert(adresa, contentValue);
        }
        saveOnWebService();
    }

    private void saveOnWebService() {
        HttpService httpService = new HttpService();
        httpService.execute(null, "POST", "http://rma20-app-rmaws.apps.us-west-1.starter.openshift-online.com/account/190a54f8-b2a5-4221-b9fe-500726a50a30");
    }
}
