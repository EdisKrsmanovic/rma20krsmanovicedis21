package edis.krsmanovic.spirala.Graphs;

import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;

import androidx.fragment.app.Fragment;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import edis.krsmanovic.spirala.DateUtil;
import edis.krsmanovic.spirala.MainActivity;
import edis.krsmanovic.spirala.R;
import edis.krsmanovic.spirala.TransactionList.Transaction;

public class GraphsFragment extends Fragment {
    private static int monthNumberDisplayed = 0;
    private static int yearNumberDisplayed = 2020;
    private static String chosenDisplay = "Monthly";

    private double[] potrosenoAmounts = new double[31];
    private double[] zaradjenoAmounts = new double[31];
    private double[] ukupnoAmounts = new double[31];

    private static double[] potrosenoMonthlyAmount = new double[12];
    private static double[] zaradjenoMonthlyAmount = new double[12];
    private static double[] ukupnoMonthlyAmount = new double[12];

    private static double[] potrosenoWeeklyAmount = new double[6];
    private static double[] zaradjenoWeeklyAmount = new double[6];
    private static double[] ukupnoWeeklyAmount = new double[6];

    private static double[] potrosenoDailyAmount = new double[31];
    private static double[] zaradjenoDailyAmount = new double[31];
    private static double[] ukupnoDailyAmount = new double[31];

    BarChart chartPotrosnja;
    BarChart chartZarada;
    BarChart chartUkupno;
    RadioGroup radioGroup;
    DatePicker datePicker;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.transactions_graph, container, false);
        chartPotrosnja = view.findViewById(R.id.barchartPotrosnja);
        chartZarada = view.findViewById(R.id.barchartZarada);
        chartUkupno = view.findViewById(R.id.barchartUkupno);
        radioGroup = view.findViewById(R.id.radioGroup);
        radioGroup.setOnCheckedChangeListener((group, checkedId) -> {
            RadioButton radioButton = view.findViewById(checkedId);
            chosenDisplay = radioButton.getText().toString();
            reloadCharts();
        });
        datePicker = view.findViewById(R.id.datePicker);
        datePicker.init(2020, 3, 1, (view2, year, monthOfYear, dayOfMonth) -> changeDate(year, monthOfYear, dayOfMonth));

        reloadCharts();

        return view;
    }

    private void changeDate(int year, int monthOfYear, int dayOfMonth) {
        if (monthOfYear != monthNumberDisplayed || year != yearNumberDisplayed) {
            monthNumberDisplayed = monthOfYear;
            yearNumberDisplayed = year;
            calculateAmounts();
            reloadCharts();
        }
    }

    private void reloadCharts() {
        if (chosenDisplay.equals("Daily")) {
            potrosenoAmounts = potrosenoDailyAmount;
            zaradjenoAmounts = zaradjenoDailyAmount;
            ukupnoAmounts = ukupnoDailyAmount;

        } else if (chosenDisplay.equals("Weekly")) {
            potrosenoAmounts = potrosenoWeeklyAmount;
            zaradjenoAmounts = zaradjenoWeeklyAmount;
            ukupnoAmounts = ukupnoWeeklyAmount;
        } else {
            potrosenoAmounts = potrosenoMonthlyAmount;
            zaradjenoAmounts = zaradjenoMonthlyAmount;
            ukupnoAmounts = ukupnoMonthlyAmount;
        }
        setChartValues(chartPotrosnja, potrosenoAmounts, Color.RED, "Graf potrosnje", "Potrosnja");
        setChartValues(chartZarada, zaradjenoAmounts, Color.parseColor("#00bfff"), "Graf zarade", "Zarada");
        setChartValues(chartUkupno, ukupnoAmounts, Color.GREEN, "Graf ukupnog stanja", "Ukupno stanje");
    }

    private void setChartValues(BarChart chart, double[] values, int color, String descriptionText, String label) {
        Description description = new Description();
        description.setText(descriptionText);
        chart.setDescription(description);

        List<BarEntry> entries = new ArrayList<>();
        List<String> xAxisLables = new ArrayList<>();
        int j=0;
        for (int i = 0; i < values.length; i++) {
//            if (values[i] != 0) {
                entries.add(new BarEntry(j, (float) values[i]));
                xAxisLables.add(String.valueOf(i+1));
                j++;
//            }
        }
        BarDataSet set = new BarDataSet(entries, label);
        set.setColors(color);
        BarData data = new BarData(set);
        data.setBarWidth(0.9f);
        chart.setData(data);
        chart.setDoubleTapToZoomEnabled(false);
        chart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
        chart.getXAxis().setLabelCount(entries.size());
        chart.getXAxis().setValueFormatter(new IndexAxisValueFormatter(xAxisLables));
        chart.setFitBars(true);
        chart.invalidate();
    }

    public void calculateAmounts() {
        Arrays.fill(potrosenoMonthlyAmount, 0d);
        Arrays.fill(zaradjenoMonthlyAmount, 0d);
        Arrays.fill(ukupnoMonthlyAmount, 0d);
        Arrays.fill(potrosenoWeeklyAmount, 0d);
        Arrays.fill(zaradjenoWeeklyAmount, 0d);
        Arrays.fill(ukupnoWeeklyAmount, 0d);
        Arrays.fill(potrosenoDailyAmount, 0d);
        Arrays.fill(zaradjenoDailyAmount, 0d);
        Arrays.fill(ukupnoDailyAmount, 0d);

        for (Transaction transaction : MainActivity.account.getTransactionList()) {
            Date startDate = transaction.getDate();
            int monthNumber = DateUtil.getMonth(startDate) - 1;
            int weekNumber = DateUtil.getWeekOfMonth(startDate) - 1;
            int dayNumber = DateUtil.getDay(startDate) - 1;
            Double amount = transaction.getAmount();
            boolean isIncome = transaction.getType().toString().toLowerCase().contains("income");
            if (!transaction.getType().toString().toLowerCase().contains("regular")) {
                if (DateUtil.getYear(startDate) == yearNumberDisplayed) {
                    if (isIncome) {
                        zaradjenoMonthlyAmount[monthNumber] += amount;
                        if (monthNumber == monthNumberDisplayed) {
                            zaradjenoWeeklyAmount[weekNumber] += amount;
                            ukupnoWeeklyAmount[weekNumber] += amount;

                            zaradjenoDailyAmount[dayNumber] += amount;
                            ukupnoDailyAmount[dayNumber] += amount;
                        }
                    } else {
                        potrosenoMonthlyAmount[monthNumber] += amount;
                        if (monthNumber == monthNumberDisplayed) {
                            potrosenoWeeklyAmount[weekNumber] += amount;
                            ukupnoWeeklyAmount[weekNumber] += amount;

                            potrosenoDailyAmount[dayNumber] += amount;
                            ukupnoDailyAmount[dayNumber] += amount;
                        }
                    }
                    ukupnoMonthlyAmount[monthNumber] += amount;
                }
            } else {
                Date endDate = transaction.getEndDate();
                if (DateUtil.getYear(endDate) >= yearNumberDisplayed) {
                    while (startDate.before(endDate)) {
                        if (DateUtil.getYear(startDate) == yearNumberDisplayed) {
                            if (isIncome) {
                                zaradjenoMonthlyAmount[monthNumber] += amount;
                                if (monthNumber == monthNumberDisplayed) {
                                    zaradjenoWeeklyAmount[weekNumber] += amount;
                                    ukupnoWeeklyAmount[weekNumber] += amount;

                                    zaradjenoDailyAmount[dayNumber] += amount;
                                    ukupnoDailyAmount[dayNumber] += amount;
                                }
                            } else {
                                potrosenoMonthlyAmount[monthNumber] += amount;
                                if (monthNumber == monthNumberDisplayed) {
                                    potrosenoWeeklyAmount[weekNumber] += amount;
                                    ukupnoWeeklyAmount[weekNumber] += amount;

                                    potrosenoDailyAmount[dayNumber] += amount;
                                    ukupnoDailyAmount[dayNumber] += amount;
                                }
                            }
                            ukupnoMonthlyAmount[monthNumber] += amount;
                        }
                        startDate = DateUtil.advanceDays(startDate, transaction.getTransactionInterval());
                        monthNumber = DateUtil.getMonth(startDate) - 1;
                        weekNumber = DateUtil.getWeekOfMonth(startDate) - 1;
                        dayNumber = DateUtil.getDay(startDate) - 1;
                    }
                }
            }
        }
    }
}
