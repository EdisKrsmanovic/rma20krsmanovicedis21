package edis.krsmanovic.spirala.TransactionDetail;

import edis.krsmanovic.spirala.TransactionList.Transaction;

interface ITransactionDetailView {
    void showTransaction(Transaction transaction);
}
