package edis.krsmanovic.spirala.TransactionDetail;

import edis.krsmanovic.spirala.TransactionList.Transaction;

public interface ITransactionDetailInteractor {
    Transaction getTransaction();
}
