package edis.krsmanovic.spirala.TransactionDetail;

import androidx.activity.OnBackPressedCallback;
import androidx.fragment.app.Fragment;

import android.app.AlertDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.Spinner;

import com.google.android.material.textfield.TextInputEditText;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import edis.krsmanovic.spirala.DateUtil;
import edis.krsmanovic.spirala.FilterSpinnerAdapter;
import edis.krsmanovic.spirala.R;
import edis.krsmanovic.spirala.TransactionList.Transaction;
import edis.krsmanovic.spirala.TransactionList.TransactionType;

public class TransactionDetailFragment extends Fragment implements ITransactionDetailView {

    private TransactionDetailPresenter transactionDetailPresenter;
    private List<TransactionType> transactionTypes = new ArrayList<>();
    private Transaction oldTransaction;
    private Transaction newTransaction;
    private String action;
    private ITransactionDetailPresenter presenter;
    private TransactionHandler transactionHandler;

    {
        transactionTypes.add(TransactionType.INDIVIDUALPAYMENT);
        transactionTypes.add(TransactionType.REGULARPAYMENT);
        transactionTypes.add(TransactionType.PURCHASE);
        transactionTypes.add(TransactionType.INDIVIDUALINCOME);
        transactionTypes.add(TransactionType.REGULARINCOME);
    }

    DatePicker datePicker;
    TextInputEditText amountText;
    TextInputEditText titleText;
    Spinner typeSpinner;
    TextInputEditText descriptionText;
    TextInputEditText intervalText;
    DatePicker endDatePicker;
    Button saveButton;
    Button deleteButton;

    public ITransactionDetailPresenter getPresenter() {
        if (presenter == null) {
            presenter = new TransactionDetailPresenter(getArguments(), this);
        }
        return presenter;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_detail, container, false);
        if (getArguments() != null && getArguments().containsKey("TRANSACTION")) {
            getPresenter().setTransaction((Transaction) getArguments().getSerializable("TRANSACTION"));
            OnBackPressedCallback callback = new OnBackPressedCallback(true /* enabled by default */) {
                @Override
                public void handleOnBackPressed() {
                    transactionHandler.cancelAction();
                }
            };

            requireActivity().getOnBackPressedDispatcher().addCallback(this, callback);
            transactionHandler = (TransactionDetailFragment.TransactionHandler) getActivity();
            datePicker = (DatePicker) view.findViewById(R.id.datePicker);
            amountText = (TextInputEditText) view.findViewById(R.id.amountText);
            titleText = (TextInputEditText) view.findViewById(R.id.titleTransactionText);
            typeSpinner = (Spinner) view.findViewById(R.id.typeSpinner);
            descriptionText = (TextInputEditText) view.findViewById(R.id.descriptionText);
            intervalText = (TextInputEditText) view.findViewById(R.id.intervalText);
            endDatePicker = (DatePicker) view.findViewById(R.id.endDatePicker);
            saveButton = (Button) view.findViewById(R.id.saveButton);
            saveButton.setOnClickListener(v -> saveTransaction());
            deleteButton = (Button) view.findViewById(R.id.deleteButton);
            deleteButton.setOnClickListener(v -> deleteTransaction());

            setTransactionTypes();
            getPresenter().showTransaction();
        }
        return view;
    }


    private void saveTransaction() {
        if (!hasErrors()) {
            Double totalLimit = getPresenter().getTotalLimit();
            Double monthLimit = getPresenter().getMonthLimit();
            Double totalAmount = getPresenter().getTotalAmount();
            List<Transaction> allTransactions = getPresenter().getAllTransactions();
            if (action.equals("EDIT")) {
                if (!oldTransaction.getType().toString().toLowerCase().contains("regular")) {
                    totalAmount -= oldTransaction.getAmount();
                } else {
                    Date startDate = oldTransaction.getDate();
                    Date endDate = oldTransaction.getEndDate();
                    while (startDate.before(endDate)) {
                        totalAmount -= oldTransaction.getAmount();
                        startDate = DateUtil.advanceDays(startDate, oldTransaction.getTransactionInterval());
                    }
                }
            }

            if (!newTransaction.getType().toString().toLowerCase().contains("regular")) {
                Double monthAmount = calculateAmountForMonthWithoutNewTransaction(newTransaction.getDate(), allTransactions);
                monthAmount += newTransaction.getAmount();
                totalAmount += monthAmount;
                if (monthAmount > monthLimit) {
                    monthLimitAlert(monthAmount, monthLimit, newTransaction.getDate());
                } else if (totalAmount > totalLimit) {
                    totalLimitAlert(totalAmount, totalLimit, newTransaction.getDate());
                } else {
                    returnAndSaveTransaction();
                }
            } else {
                Date startDate = newTransaction.getDate();
                Date endDate = newTransaction.getEndDate();
                Double monthAmount = calculateAmountForMonthWithoutNewTransaction(startDate, allTransactions);
                while (startDate.before(endDate)) {
                    Date newStartDate = DateUtil.advanceDays(startDate, newTransaction.getTransactionInterval());
                    if (DateUtil.getMonth(startDate) != DateUtil.getMonth(newStartDate)) {
                        if (monthAmount > monthLimit) {
                            monthLimitAlert(monthAmount, monthLimit, newTransaction.getDate());
                            return;
                        } else {
                            monthAmount = calculateAmountForMonthWithoutNewTransaction(startDate, allTransactions);
                        }
                    } else {
                        monthAmount += newTransaction.getAmount();
                        totalAmount += newTransaction.getAmount();
                    }
                    startDate = newStartDate;
                }
                if (totalAmount > totalLimit) {
                    totalLimitAlert(totalAmount, totalLimit, newTransaction.getDate());
                } else {
                    returnAndSaveTransaction();
                }
            }
        } else {
            new AlertDialog.Builder(getContext())
                    .setTitle("Errors")
                    .setMessage("Fix all fields that have red background first")
                    .setPositiveButton(android.R.string.ok, null)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
        }
    }

    private void monthLimitAlert(Double current, Double limit, Date date) {
        new AlertDialog.Builder(getContext())
                .setTitle("Warning")
                .setMessage(String.format("You have surpassed monthly limit for month %s %d. \n" +
                                "Month limit: %.2f \n" +
                                "New month total: %.2f \n" +
                                "Are you sure you want to continue?",
                        DateUtil.getMonthName(newTransaction.getDate()), DateUtil.getYear(newTransaction.getDate()), limit, current))
                .setPositiveButton("Yes", (dialog, which) -> returnAndSaveTransaction())
                .setNegativeButton("No", null)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    private void totalLimitAlert(Double current, Double limit, Date date) {
        new AlertDialog.Builder(getContext())
                .setTitle("Warning")
                .setMessage(String.format("You have surpassed total limit. \n" +
                        "Total limit: %.2f \n" +
                        "New total: %.2f \n" +
                        "Are you sure you want to continue?", limit, current))
                .setPositiveButton("Yes", (dialog, which) -> returnAndSaveTransaction())
                .setNegativeButton("No", null)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    private void returnAndSaveTransaction() {
        String action = getArguments().getString("ACTION");
        if (action.equalsIgnoreCase("EDIT")) {
            transactionHandler.saveTransaction(newTransaction);
        } else {
            transactionHandler.addNewTransaction(newTransaction);
        }
    }

    private Double calculateAmountForMonthWithoutNewTransaction(Date date, List<Transaction> transactions) {
        double sum = 0d;
        for (Transaction transaction : transactions) {
            if (!transaction.getId().equals(newTransaction.getId())) {
                Date startDate = transaction.getDate();
                if (!transaction.getType().toString().toLowerCase().contains("regular")) {
                    if (DateUtil.getMonth(startDate) == DateUtil.getMonth(date) && DateUtil.getYear(startDate) == DateUtil.getYear(date)) {
                        sum += transaction.getAmount();
                    }
                } else {
                    Date endDate = transaction.getEndDate();
                    while (startDate.before(endDate)) {
                        if (DateUtil.getMonth(startDate) == DateUtil.getMonth(date) && DateUtil.getYear(startDate) == DateUtil.getYear(date)) {
                            sum += transaction.getAmount();
                        }
                        startDate = DateUtil.advanceDays(startDate, transaction.getTransactionInterval());
                    }
                }
            }
        }
        return sum;
    }

    private void deleteTransaction() {
        new AlertDialog.Builder(getContext())
                .setTitle("Delete dialog")
                .setMessage("Are you sure you want to delete this dialog?")

                .setPositiveButton(android.R.string.yes, (dialog, which) -> {
                    transactionHandler.deleteTransaction(newTransaction);
                })
                .setNegativeButton(android.R.string.no, null)
                .setIcon(android.R.drawable.ic_dialog_info)
                .show();
    }

    private boolean hasErrors() {
        return getColor(amountText) == Color.RED || getColor(titleText) == Color.RED || getColor(intervalText) == Color.RED || getColor(endDatePicker) == Color.RED;
    }

    private int getColor(View view) {
        Drawable background = view.getBackground();
        if (background instanceof ColorDrawable) return ((ColorDrawable) background).getColor();
        else return Color.TRANSPARENT;
    }

    @Override
    public void showTransaction(Transaction transaction) {
        oldTransaction = transaction;
        newTransaction = new Transaction(transaction);
        action = getArguments().getString("ACTION");
        if (!action.equals("EDIT")) {
            deleteButton.setEnabled(false);
        }
        showDate(transaction);
        showAmount(transaction);
        showTitle(transaction);
        showType(transaction);
        showDescription(transaction);
        showInterval(transaction);
        showEndDate(transaction);
        if (!transaction.getType().toString().toLowerCase().contains("income")) {
            descriptionText.setEnabled(true);
        } else {
            descriptionText.setEnabled(false);
        }
        if (transaction.getType().toString().toLowerCase().contains("regular")) {
            intervalText.setEnabled(true);
            endDatePicker.setEnabled(true);
        } else {
            intervalText.setEnabled(false);
            endDatePicker.setEnabled(false);
        }
    }

    private void showDate(Transaction transaction) {
        Date date = transaction.getDate();
        datePicker.init(DateUtil.getYear(date), DateUtil.getMonth(date) - 1, DateUtil.getDay(date), (view, year, monthOfYear, dayOfMonth) -> changeDate(year, monthOfYear, dayOfMonth));
    }

    private void changeDate(int year, int monthOfYear, int dayOfMonth) {
        Date oldDate = oldTransaction.getDate();
        if (DateUtil.getDay(oldDate) != dayOfMonth || DateUtil.getMonth(oldDate) != monthOfYear + 1 || DateUtil.getYear(oldDate) != year) {
            datePicker.setBackgroundColor(Color.GREEN);
        } else {
            datePicker.setBackgroundColor(Color.TRANSPARENT);
        }
        newTransaction.setDate(DateUtil.getDate(year, monthOfYear + 1, dayOfMonth));
    }

    private void showAmount(Transaction transaction) {
        if (transaction.getAmount() == null) {
            amountText.setText("0");
            newTransaction.setAmount(0d);
        } else amountText.setText(String.format("%.2f", transaction.getAmount()));
        amountText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                changeAmount();
            }
        });
    }

    private void changeAmount() {
        try {
            Double newAmount = Double.parseDouble(amountText.getText().toString());
            if (!newAmount.equals(oldTransaction.getAmount())) {
                amountText.setBackgroundColor(Color.GREEN);
            } else {
                amountText.setBackgroundColor(Color.TRANSPARENT);
            }
            newTransaction.setAmount(newAmount);
        } catch (NumberFormatException ex) {
            amountText.setBackgroundColor(Color.RED);
        }
    }

    private void showTitle(Transaction transaction) {
        if (transaction.getTitle() == null) {
            changeTitle();
        } else titleText.setText(transaction.getTitle());
        titleText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                changeTitle();
            }
        });
    }

    private void changeTitle() {
        String title = titleText.getText().toString();
        if (title.length() <= 3 || title.length() >= 15) {
            titleText.setBackgroundColor(Color.RED);
        } else if (!title.equals(oldTransaction.getTitle())) {
            titleText.setBackgroundColor(Color.GREEN);
        } else {
            titleText.setBackgroundColor(Color.TRANSPARENT);
        }
        if (!(title.length() <= 3 && title.length() >= 15)) newTransaction.setTitle(title);
    }

    private void showType(Transaction transaction) {
        typeSpinner.setSelection(transactionTypes.indexOf(transaction.getType()));
        typeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                changeType();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void changeType() {
        TransactionType selectedType = (TransactionType) typeSpinner.getItemAtPosition((int) typeSpinner.getSelectedItemId() + 1);
        if (selectedType != null) {
            TransactionType transactionType = TransactionType.valueOf(selectedType.toString());
            if (transactionType != oldTransaction.getType()) {
                typeSpinner.setBackgroundResource(R.drawable.spinner_background_green);
            } else {
                typeSpinner.setBackgroundResource(R.drawable.spinner_background);
            }

            if (selectedType.toString().toLowerCase().contains("income")) {
                descriptionText.setEnabled(false);
                descriptionText.setBackgroundColor(Color.TRANSPARENT);
            } else {
                descriptionText.setEnabled(true);
            }

            if (!selectedType.toString().toLowerCase().contains("regular")) {
                intervalText.setEnabled(false);
                intervalText.setText("");
                endDatePicker.setEnabled(false);
                intervalText.setBackgroundColor(Color.TRANSPARENT);
                endDatePicker.setBackgroundColor(Color.TRANSPARENT);
            } else {
                intervalText.setEnabled(true);
                endDatePicker.setEnabled(true);
                Date endDate = oldTransaction.getEndDate();
                if (endDate == null) {
                    changeEndDate(2020, 3, 25);
                } else {
                    changeEndDate(DateUtil.getYear(endDate), DateUtil.getMonth(endDate) - 1, DateUtil.getDay(endDate));
                }
                showInterval(newTransaction);
            }
            newTransaction.setType(selectedType);
        }
    }

    private void showDescription(Transaction transaction) {
        descriptionText.setText(transaction.getItemDescription());
        descriptionText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                changeDescription();
            }
        });
    }

    private void changeDescription() {
        String description = descriptionText.getText().toString();
        if (!description.equals(oldTransaction.getItemDescription())) {
            descriptionText.setBackgroundColor(Color.GREEN);
        } else {
            descriptionText.setBackgroundColor(Color.TRANSPARENT);
        }
        newTransaction.setItemDescription(description);
    }

    private void showInterval(Transaction transaction) {
        if (transaction.getTransactionInterval() == null) {
            intervalText.setText("1");
            newTransaction.setTransactionInterval(1);
        } else intervalText.setText(String.format("%d", transaction.getTransactionInterval()));
        intervalText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                changeInterval();
            }
        });
    }

    private void changeInterval() {
        String intervalString = intervalText.getText().toString();
        try {
            Integer newInterval = Integer.parseInt(intervalString);
            if (newInterval <= 0) {
                intervalText.setBackgroundColor(Color.RED);
            } else if (!newInterval.equals(oldTransaction.getTransactionInterval())) {
                intervalText.setBackgroundColor(Color.GREEN);
            } else {
                intervalText.setBackgroundColor(Color.TRANSPARENT);
            }
            newTransaction.setTransactionInterval(newInterval);
        } catch (NumberFormatException ex) {
            intervalText.setBackgroundColor(Color.RED);
        }
    }

    private void showEndDate(Transaction transaction) {
        Date endDate = transaction.getEndDate();
        if (endDate != null) {
            endDatePicker.init(DateUtil.getYear(endDate), DateUtil.getMonth(endDate) - 1, DateUtil.getDay(endDate), (view, year, monthOfYear, dayOfMonth) -> changeEndDate(year, monthOfYear, dayOfMonth));
        } else {
            endDatePicker.init(2020, 3, 25, (view, year, monthOfYear, dayOfMonth) -> changeEndDate(year, monthOfYear, dayOfMonth));
        }
    }

    private void changeEndDate(int year, int monthOfYear, int dayOfMonth) {
        Date oldEndDate = oldTransaction.getEndDate();
        if (oldEndDate == null || (DateUtil.getDay(oldEndDate) != dayOfMonth || DateUtil.getMonth(oldEndDate) != (monthOfYear + 1) || DateUtil.getYear(oldEndDate) != year)) {
            endDatePicker.setBackgroundColor(Color.GREEN);
        } else {
            endDatePicker.setBackgroundColor(Color.TRANSPARENT);
        }
        newTransaction.setEndDate(DateUtil.getDate(year, monthOfYear + 1, dayOfMonth));
    }

    private void setTransactionTypes() {
        List<String> objects = new ArrayList<String>();
        objects.add("Individual payment");
        objects.add("Regular payment");
        objects.add("Purchase");
        objects.add("Individual income");
        objects.add("Regular income");
        objects.add("Transaction type");
        FilterSpinnerAdapter adapter = new FilterSpinnerAdapter(getContext(), objects);
        typeSpinner.setAdapter(adapter);
    }

    public interface TransactionHandler {
        public void saveTransaction(Transaction transaction);

        public void addNewTransaction(Transaction newTransaction);

        public void deleteTransaction(Transaction transaction);

        public void cancelAction();
    }
}
