package edis.krsmanovic.spirala.TransactionDetail;

import android.content.Intent;
import android.os.Bundle;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import edis.krsmanovic.spirala.MainActivity;
import edis.krsmanovic.spirala.TransactionList.Transaction;

public class TransactionDetailPresenter implements ITransactionDetailPresenter {

    private TransactionDetailInteractor transactionDetailInteractor;
    private ITransactionDetailView transactionDetailView;
    private Bundle arguments;
    private Transaction transaction;

    public TransactionDetailPresenter(Bundle arguments, ITransactionDetailView view) {
        this.transactionDetailInteractor = new TransactionDetailInteractor(arguments);
        this.transactionDetailView = view;
        this.arguments = arguments;
    }

    @Override
    public void showTransaction() {
        Transaction transaction = transactionDetailInteractor.getTransaction();
        transactionDetailView.showTransaction(transaction);
    }

    @Override
    public Double getTotalLimit() {
//        return arguments.getDouble("TOTALLIMIT", 0);
        return MainActivity.account.getTotalLimit();
    }

    @Override
    public Double getMonthLimit() {
//        return arguments.getDouble("MONTHLIMIT", 0);
        return MainActivity.account.getMonthLimit();
    }

    @Override
    public Double getTotalAmount() {
        return arguments.getDouble("CURRENTTOTALAMOUNT", 0);
    }

    @Override
    public Double getMonthAmount() {
        return arguments.getDouble("CURRENTMONTHAMOUNT", 0);
    }

    @Override
    public List<Transaction> getAllTransactions() {
        return new ArrayList<Transaction>(arguments.getParcelableArrayList("ALLTRANSACTIONS"));
    }

    @Override
    public void setTransaction(Transaction transaction) {
        this.transaction = transaction;
    }
}
