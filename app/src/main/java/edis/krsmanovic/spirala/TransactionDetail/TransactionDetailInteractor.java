package edis.krsmanovic.spirala.TransactionDetail;

import android.content.Intent;
import android.os.Bundle;

import edis.krsmanovic.spirala.TransactionList.Transaction;

public class TransactionDetailInteractor implements ITransactionDetailInteractor {
    private Bundle arguments;

    public TransactionDetailInteractor(Bundle arguments) {
        this.arguments = arguments;
    }

    @Override
    public Transaction getTransaction() {
        return (Transaction) arguments.getSerializable("TRANSACTION");
    }
}
