package edis.krsmanovic.spirala.TransactionDetail;

import java.io.Serializable;
import java.util.List;

import edis.krsmanovic.spirala.TransactionList.Transaction;

public interface ITransactionDetailPresenter {
    void showTransaction();
    Double getTotalLimit();
    Double getMonthLimit();
    Double getTotalAmount();
    Double getMonthAmount();
    List<Transaction> getAllTransactions();

    void setTransaction(Transaction transaction);
}

