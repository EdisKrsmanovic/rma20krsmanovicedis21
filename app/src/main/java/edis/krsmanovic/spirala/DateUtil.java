package edis.krsmanovic.spirala;

import android.text.format.DateFormat;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class DateUtil {
    public static Date increaseMonth(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.MONTH, 1);
        return cal.getTime();
    }

    public static Date decreaseMonth(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.MONTH, -1);
        return cal.getTime();
    }

    public static Date getDate(int year, int month, int day) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.MONTH, month - 1);
        cal.set(Calendar.DAY_OF_MONTH, day);
        return cal.getTime();
    }

    public static Date getDate(String dateString) {
        if(dateString == null) return null;
        try {
            SimpleDateFormat localDateFormat = new SimpleDateFormat("MMM dd yyyy");
            return localDateFormat.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static int getDay(Date date) {
        String dayNumber = (String) DateFormat.format("dd", date);
        return Integer.parseInt(dayNumber);
    }

    public static int getWeekOfMonth(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal.get(Calendar.WEEK_OF_MONTH);
    }

    public static int getMonth(Date date) {
        String monthNumber = (String) DateFormat.format("MM", date);
        return Integer.parseInt(monthNumber);
    }

    public static int getYear(Date date) {
        String year = (String) DateFormat.format("yyyy", date);
        return Integer.parseInt(year);
    }

    public static String getMonthName(Date date) {
        return (String) DateFormat.format("MMMM", date);
    }

    public static long daysBetween(Date date1, Date date2) {
        long diff = Math.abs(date1.getTime() - date2.getTime());
        return TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
    }

    public static Date advanceDays(Date date, int days) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, days);
        return cal.getTime();
    }

    public static String toString(Date date) {
        if(date == null) return null;
        String pattern = "MMM dd yyyy";
        SimpleDateFormat df = new SimpleDateFormat(pattern);
        return df.format(date);
    }
}
