package edis.krsmanovic.spirala.TransactionList;

import java.util.List;

public interface ITransactionListPresenter {
    void refreshAll();
    void saveTransaction(Transaction transaction);
    void deleteTransaction(Integer id);
    List<Transaction> getAll();
    void addNewTransaction(Transaction transaction);
}
