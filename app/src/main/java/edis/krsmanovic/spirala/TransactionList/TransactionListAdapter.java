package edis.krsmanovic.spirala.TransactionList;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import edis.krsmanovic.spirala.MainActivity;
import edis.krsmanovic.spirala.R;

public class TransactionListAdapter extends ArrayAdapter<Transaction> {
    private final Activity context;
    private List<Transaction> transactionList;

    public TransactionListAdapter(Activity context, List<Transaction> transactionList) {
        super(context, R.layout.transaction_row, transactionList);
        this.context = context;
        this.transactionList = transactionList;
    }

    public void setTransactions(List<Transaction> transactionList) {
        this.addAll(transactionList);
    }

//    @Override
//    public Transaction getItem(int position) {
//        return transactionList.get(position);
//    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.transaction_row, null, true);

        Transaction transaction = getItem(position);

        TextView titleText = (TextView) rowView.findViewById(R.id.titleText);
        titleText.setText(transaction.getTitle());

        TextView amountText = (TextView) rowView.findViewById(R.id.amountText);
        amountText.setText(String.format("%.2f", transaction.getAmount()));

        ImageView transactionIcon = (ImageView) rowView.findViewById(R.id.transactionIcon);

        if (transaction.getType() == TransactionType.INDIVIDUALPAYMENT) {
            transactionIcon.setImageResource(R.drawable.ic_individualpayment);
        } else if (transaction.getType() == TransactionType.REGULARPAYMENT) {
            transactionIcon.setImageResource(R.drawable.ic_regularpayment);
        } else if (transaction.getType() == TransactionType.PURCHASE) {
            transactionIcon.setImageResource(R.drawable.ic_purchase);
        } else if (transaction.getType() == TransactionType.INDIVIDUALINCOME) {
            transactionIcon.setImageResource(R.drawable.ic_individualincome);
        } else if (transaction.getType() == TransactionType.REGULARINCOME) {
            transactionIcon.setImageResource(R.drawable.ic_regularincome);
        }

        return rowView;
    }
}
