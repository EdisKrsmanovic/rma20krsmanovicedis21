package edis.krsmanovic.spirala.TransactionList;

import java.io.Serializable;
import java.util.Date;

import edis.krsmanovic.spirala.DateUtil;

public class Transaction implements Serializable {
    private Integer id; //internal id
    private Date date;
    private Double amount;
    private String title;
    private TransactionType type;
    private String itemDescription;
    private Integer transactionInterval;    //Only for REGULAR... transaction types
    private Date endDate;                   //Only for REGULAR... transaction types

    private Integer externalId = null; //njihov id

    public Transaction(Integer id) {
        this.id = id;
        this.date = DateUtil.getDate(2020, 3, 25);
        this.amount = null;
        this.title = null;
        this.type = TransactionType.INDIVIDUALINCOME;
        this.itemDescription = null;
        this.transactionInterval = null;
        this.endDate = null;
    }

    public Transaction(Integer id, Date date, Double amount, String title, TransactionType type, String itemDescription, Integer transactionInterval, Date endDate) {
//        if (title.length() <= 3 || title.length() >= 15) {
//            throw new InvalidTitleException(String.format("Transactions title '%s' is invalid, its length must be >=3 and <=15", title));
//        }
        this.id = id;
        this.date = date;
        this.amount = amount;
        this.title = title;
        this.type = type;
        this.itemDescription = itemDescription;
        this.transactionInterval = transactionInterval;
        this.endDate = endDate;
    }

    public Transaction(Integer internalId, Integer externalId, Date date, Double amount, String title, TransactionType type, String itemDescription, Integer transactionInterval, Date endDate) {
        this.id = internalId;
        this.externalId = externalId;
        this.date = date;
        this.amount = amount;
        this.title = title;
        this.type = type;
        this.itemDescription = itemDescription;
        this.transactionInterval = transactionInterval;
        this.endDate = endDate;
    }

    public Transaction(Transaction transaction) {
        this.id = transaction.id;
        this.date = transaction.date;
        this.amount = transaction.amount;
        this.title = transaction.title;
        this.type = transaction.type;
        this.itemDescription = transaction.itemDescription;
        this.transactionInterval = transaction.transactionInterval;
        this.endDate = transaction.endDate;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public TransactionType getType() {
        return type;
    }

    public void setType(TransactionType type) {
        this.type = type;
    }

    public String getItemDescription() {
        return itemDescription;
    }

    public void setItemDescription(String itemDescription) {
        this.itemDescription = itemDescription;
    }

    public Integer getTransactionInterval() {
        return transactionInterval;
    }

    public void setTransactionInterval(Integer transactionInterval) {
        this.transactionInterval = transactionInterval;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Integer getExternalId() {
        return externalId;
    }

    public void setExternalId(Integer id2) {
        this.externalId = id2;
    }
}
