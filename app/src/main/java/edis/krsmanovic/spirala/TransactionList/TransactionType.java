package edis.krsmanovic.spirala.TransactionList;

public enum TransactionType {
    INDIVIDUALPAYMENT,
    REGULARPAYMENT,
    PURCHASE,
    INDIVIDUALINCOME,
    REGULARINCOME
}
