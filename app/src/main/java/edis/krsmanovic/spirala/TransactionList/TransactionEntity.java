package edis.krsmanovic.spirala.TransactionList;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class TransactionEntity {
    private int TransactionTypeId;
    private Date date;
    private Double amount;
    private String title;
    private String itemDescription;
    private Integer transactionInterval;    //Only for REGULAR... transaction types
    private Date endDate;                   //Only for REGULAR... transaction types

    public TransactionEntity(Transaction transaction) {
        List<TransactionType> transactionTypeList = new ArrayList<>();
        transactionTypeList.add(TransactionType.REGULARPAYMENT);
        transactionTypeList.add(TransactionType.REGULARINCOME);
        transactionTypeList.add(TransactionType.PURCHASE);
        transactionTypeList.add(TransactionType.INDIVIDUALINCOME);
        transactionTypeList.add(TransactionType.INDIVIDUALPAYMENT);
        TransactionTypeId = transactionTypeList.indexOf(transaction.getType()) + 1;
        date = transaction.getDate();
        amount = transaction.getAmount();
        title = transaction.getTitle();
        itemDescription = transaction.getItemDescription();
        transactionInterval = transaction.getTransactionInterval();
        endDate = transaction.getEndDate();
    }

    public int getTransactionTypeId() {
        return TransactionTypeId;
    }

    public void setTransactionTypeId(int transactionTypeId) {
        TransactionTypeId = transactionTypeId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getItemDescription() {
        return itemDescription;
    }

    public void setItemDescription(String itemDescription) {
        this.itemDescription = itemDescription;
    }

    public Integer getTransactionInterval() {
        return transactionInterval;
    }

    public void setTransactionInterval(Integer transactionInterval) {
        this.transactionInterval = transactionInterval;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }
}
