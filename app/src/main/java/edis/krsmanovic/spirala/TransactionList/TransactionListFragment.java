package edis.krsmanovic.spirala.TransactionList;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import edis.krsmanovic.spirala.Account.Account;
import edis.krsmanovic.spirala.DateUtil;
import edis.krsmanovic.spirala.FilterSpinnerAdapter;
import edis.krsmanovic.spirala.MainActivity;
import edis.krsmanovic.spirala.R;
import edis.krsmanovic.spirala.SortSpinnerAdapter;

public class TransactionListFragment extends Fragment implements ITransactionListView {
    private boolean shouldRefresh;
    private ITransactionListPresenter transactionListPresenter;
    private TransactionListAdapter transactionListAdapter;
    private TransactionType filter = null;
    //    private Comparator<Transaction> sortCriteria = null;
    private String sortCriteria = null;
    private static List<Transaction> currentTransactions;
    private TransactionListHandler transactionListHandler;

    ListView listView;
    Spinner spinnerFilterBy;
    Spinner spinnerSortBy;
    Date dateDisplayed = DateUtil.getDate(2020, 3, 25);
    TextView dateTextField;
    FloatingActionButton decreaseMonthButton;
    FloatingActionButton increaseMonthButton;
    TextView globalAmountText;
    TextView limitText;
    TextView offlineIzmjenaText;
    Button addTransaction;

    public TransactionListFragment() {
        this.shouldRefresh = true;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View fragmentView = inflater.inflate(R.layout.fragment_list, container, false);

        transactionListAdapter = new TransactionListAdapter(getActivity(), new ArrayList<>());
        listView = (ListView) fragmentView.findViewById(R.id.listview);
        listView.setAdapter(transactionListAdapter);
        transactionListHandler = (TransactionListHandler) getActivity();
        listView.setOnItemClickListener(listItemClickListener);
        spinnerSortBy = (Spinner) fragmentView.findViewById(R.id.spinnerSortBy);
        spinnerFilterBy = (Spinner) fragmentView.findViewById(R.id.spinnerFilterBy);
        dateTextField = (TextView) fragmentView.findViewById(R.id.dateTextField);
        decreaseMonthButton = (FloatingActionButton) fragmentView.findViewById(R.id.decreaseMonthButton);
        increaseMonthButton = (FloatingActionButton) fragmentView.findViewById(R.id.increaseMonthButton);
        globalAmountText = (TextView) fragmentView.findViewById(R.id.textView2);
        limitText = (TextView) fragmentView.findViewById(R.id.textView);
        offlineIzmjenaText = (TextView) fragmentView.findViewById(R.id.offlineIzmjenaText);
        addTransaction = (Button) fragmentView.findViewById(R.id.addTransaction);
        addTransaction.setOnClickListener(v -> addButtonAction());
        setGlobalAmount();
        setTotalLimit();
        decreaseMonthButton.setOnClickListener(v -> decreaseMonth());
        increaseMonthButton.setOnClickListener(v -> increaseMonth());

        if(currentTransactions != null) {
            transactionListAdapter.setTransactions(currentTransactions);
            transactionListAdapter.notifyDataSetChanged();
        }

        refreshCurrentDate();
        setFilterByOptions();
        setSortByOptions();setOfflineTextVisibility(MainActivity.hasInternetAccess);
        return fragmentView;
    }

    public void addNewTransaction(Transaction newTransaction) {
        getPresenter().addNewTransaction(newTransaction);
        getPresenter().refreshAll();
    }

    public void saveTransaction(Transaction transaction) {
        getPresenter().saveTransaction(transaction);
        getPresenter().refreshAll();
    }

    public void deleteTransaction(Transaction transaction) {
        getPresenter().deleteTransaction(transaction.getId());
        getPresenter().refreshAll();
    }

    public void refreshAll() {
        getPresenter().refreshAll();
    }

    public void setOfflineTextVisibility(boolean hasInternetAccess) {
        if(hasInternetAccess) {
            offlineIzmjenaText.setVisibility(View.INVISIBLE);
        } else {
            offlineIzmjenaText.setVisibility(View.VISIBLE);
        }
    }

    public interface TransactionListHandler {

        public void onItemClicked(Transaction transaction, Double currentTotal, ArrayList<Transaction> allTransactions, Account account);

        public void onAddClicked(Integer id, Double currentTotal, ArrayList<Transaction> allTransactions, Account account);
    }

    private void addButtonAction() {
        transactionListHandler.onAddClicked(getPresenter().getNextId(), calculateCurrentTotalAmount(), (ArrayList<Transaction>) MainActivity.account.getTransactionList(), MainActivity.account);
    }

    public static List<Transaction> getCurrentTransactions() {
        return currentTransactions;
    }

    private Double calculateCurrentTotalAmount() {
        Double totalAmount = 0d;
        for (Transaction transaction : currentTransactions) {
            if (!transaction.getType().toString().toLowerCase().contains("regular")) {
                totalAmount += transaction.getAmount();
            } else {
                Date startDate = transaction.getDate();
                Date endDate = transaction.getEndDate();
                while (startDate.before(endDate)) {
                    totalAmount += transaction.getAmount();
                    startDate = DateUtil.advanceDays(startDate, transaction.getTransactionInterval());
                }
            }
        }
        return totalAmount;
    }

    private void setTotalLimit() {
        limitText.setText(String.format("Limit: %.2f", MainActivity.account.getTotalLimit()));
    }

    private void setGlobalAmount() {
        Double sum = 0d;
        for (Transaction transaction : MainActivity.account.getTransactionList()) {
            if (!transaction.getType().toString().toLowerCase().contains("regular")) {
                sum += transaction.getAmount();
            } else {
                Date startDate = transaction.getDate();
                Date endDate = transaction.getEndDate();
                while (startDate.before(endDate)) {
                    sum += transaction.getAmount();
                    startDate = DateUtil.advanceDays(startDate, transaction.getTransactionInterval());
                }
            }
        }
        globalAmountText.setText(String.format("Global amount: %.2f", sum));
    }

    public TransactionListPresenter getPresenter() {
        if (transactionListPresenter == null) {
            transactionListPresenter = new TransactionListPresenter(getContext(), this, MainActivity.account, transactionListAdapter);
        }
        return (TransactionListPresenter) transactionListPresenter;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        this.refreshAll();
        getPresenter().updateWebService();
    }

    private void decreaseMonth() {
        dateDisplayed = DateUtil.decreaseMonth(dateDisplayed);
        refreshCurrentDate();
        getPresenter().refreshAll();
    }

    private void increaseMonth() {
        dateDisplayed = DateUtil.increaseMonth(dateDisplayed);
        refreshCurrentDate();
        getPresenter().refreshAll();
    }

    private void refreshCurrentDate() {
        dateTextField.setText(DateUtil.getMonthName(dateDisplayed) + ", " + DateUtil.getYear(dateDisplayed));
    }

    private void setFilterByOptions() {
        List<String> objects = new ArrayList<String>();
        objects.add("All");
        objects.add("Individual payment");
        objects.add("Regular payment");
        objects.add("Purchase");
        objects.add("Individual income");
        objects.add("Regular income");
        // add hint as last item
        objects.add("Filter by");
        FilterSpinnerAdapter adapter = new FilterSpinnerAdapter(getActivity(), objects);
        spinnerFilterBy.setAdapter(adapter);
        int oldPosition = spinnerFilterBy.getSelectedItemPosition();
        spinnerFilterBy.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    filter = null;
                } else if (position != 6) {
                    filter = TransactionType.valueOf(parent.getSelectedItem().toString());
                }
                if (oldPosition != position) {
                    getPresenter().refreshAll();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                filter = null;
            }
        });
        // show hint
        spinnerFilterBy.setSelection(0);
    }

    private void setSortByOptions() {
        List<String> objects = new ArrayList<String>();
        objects.add("Sort by");
        objects.add("Price - Ascending");
        objects.add("Price - Descending");
        objects.add("Title - Ascending");
        objects.add("Title - Descending");
        objects.add("Date - Ascending");
        objects.add("Date - Descending");
        // add hint as last item
        SortSpinnerAdapter adapter = new SortSpinnerAdapter(getActivity(), objects, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerSortBy.setAdapter(adapter);
        int oldPosition = spinnerSortBy.getSelectedItemPosition();
        spinnerSortBy.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String sortString = parent.getSelectedItem().toString();
                if (!sortString.equalsIgnoreCase("sort by")) {
                    if (sortString.equalsIgnoreCase("price - ascending")) {
                        sortCriteria = "amount.asc";
                    } else if (sortString.equalsIgnoreCase("price - descending")) {
                        sortCriteria = "amount.desc";
                    } else if (sortString.equalsIgnoreCase("title - ascending")) {
                        sortCriteria = "title.asc";
                    } else if (sortString.equalsIgnoreCase("title - descending")) {
                        sortCriteria = "title.desc";
                    } else if (sortString.equalsIgnoreCase("date - ascending")) {
                        sortCriteria = "date.asc";
                    } else if (sortString.equalsIgnoreCase("date - descending")) {
                        sortCriteria = "date.desc";
                    }
                }
                if (oldPosition != position) {
                    getPresenter().refreshAll();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                sortCriteria = null;
            }
        });
        // show hint
        spinnerSortBy.setSelection(0);
    }

    @Override
    public void setTransactions(List<Transaction> transactionList) {
        transactionListAdapter.clear();
        transactionListAdapter.setTransactions(transactionList);
        transactionListAdapter.notifyDataSetChanged();
        this.currentTransactions = transactionList;
        setGlobalAmount();
    }

    @Override
    public int getMonth() {
        return DateUtil.getMonth(dateDisplayed);
    }

    @Override
    public int getYear() {
        return DateUtil.getYear(dateDisplayed);
    }

    @Override
    public TransactionType getFilter() {
        return filter;
    }

    @Override
    public String getSortCriteria() {
        return sortCriteria;
    }

    @Override
    public void notifyTransactionListDataSetChanged() {
        transactionListAdapter.notifyDataSetChanged();
    }

//    private int getColor(View view) {
//        Drawable background = view.getBackground();
//        if (background instanceof ColorDrawable) return ((ColorDrawable) background).getColor();
//        else return Color.TRANSPARENT;
//    }

    private int selectedTransactionId = -1;
    private View selectedView;

    private AdapterView.OnItemClickListener listItemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Transaction transaction = transactionListAdapter.getItem(position);

            if (selectedTransactionId == transaction.getId()) {
                selectedView.setBackgroundColor(Color.TRANSPARENT);
                selectedView = null;
                selectedTransactionId = -1;
            } else {
                if (selectedView != null) selectedView.setBackgroundColor(Color.TRANSPARENT);
                selectedTransactionId = transaction.getId();
                view.setBackgroundColor(Color.GREEN);
                selectedView = view;
            }
            transactionListHandler.onItemClicked(transaction, calculateCurrentTotalAmount(), (ArrayList<Transaction>) getPresenter().getAll(), MainActivity.account);
        }
    };

}
