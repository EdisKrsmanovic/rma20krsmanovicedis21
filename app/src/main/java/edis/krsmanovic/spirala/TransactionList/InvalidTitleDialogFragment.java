package edis.krsmanovic.spirala.TransactionList;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;

import androidx.fragment.app.DialogFragment;

public class InvalidTitleDialogFragment extends DialogFragment {
    private String message;

    public InvalidTitleDialogFragment(String message) {
        this.message = message;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(message)
                .setPositiveButton("Ok", (dialog, which) -> {});
        return builder.create();
    }
}