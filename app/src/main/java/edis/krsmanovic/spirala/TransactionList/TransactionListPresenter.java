package edis.krsmanovic.spirala.TransactionList;

import android.content.Context;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;

import edis.krsmanovic.spirala.Account.Account;
import edis.krsmanovic.spirala.DateUtil;
import edis.krsmanovic.spirala.HttpService;
import edis.krsmanovic.spirala.MainActivity;

public class TransactionListPresenter implements ITransactionListPresenter {

    private final TransactionListAdapter transactionListAdapter;
    private Context context;
    private ITransactionListView transactionListView;
    private TransactionListInteractor transactionListInteractor;
    private Account account;

    public TransactionListPresenter(Context context, ITransactionListView transactionListView, Account account, TransactionListAdapter transactionListAdapter) {
        this.context = context;
        this.transactionListView = transactionListView;
        this.transactionListInteractor = new TransactionListInteractor(account, transactionListView, context);
        this.account = account;
        this.transactionListAdapter = transactionListAdapter;
    }

    @Override
    public void refreshAll() {
        getFilteredTransactions();
    }

    public void setTransactions(List<Transaction> transactionList) {
        transactionListView.setTransactions(transactionList);
        transactionListView.notifyTransactionListDataSetChanged();
    }

    private void getFilteredTransactions() {
        if (MainActivity.hasInternetAccess) {
            new Thread(() -> {
                try {
                    HttpService httpService = new HttpService();
                    httpService.execute(this, "GET").get();
                } catch (ExecutionException | InterruptedException e) {
                    e.printStackTrace();
                }
            }).start();
        } else {
            if(MainActivity.account.getTransactionList().size() == 0) {
                MainActivity.account.setTransactionList(transactionListInteractor.getAllTransactionsFromDB());
            }
            this.setTransactions(transactionListInteractor.filterTransactionsByDate(transactionListView.getMonth(), transactionListView.getYear(), MainActivity.account.getTransactionList()));
        }
    }

    @Override
    public void saveTransaction(Transaction transaction) {
        transactionListInteractor.changeTransaction(transaction);
    }

    @Override
    public void deleteTransaction(Integer id) {
        transactionListInteractor.deleteTransaction(id);
    }

    @Override
    public List<Transaction> getAll() {
        return transactionListInteractor.getAll();
    }

    @Override
    public void addNewTransaction(Transaction transaction) {
        transactionListInteractor.addNewTransaction(transaction);
    }

    public Integer getNextId() {
        return transactionListInteractor.getNextId();
    }

    public ITransactionListView getTransactionListView() {
        return transactionListView;
    }

    public void updateWebService() {
        transactionListInteractor.updateWebService();
    }
}
