package edis.krsmanovic.spirala.TransactionList;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import edis.krsmanovic.spirala.Account.Account;
import edis.krsmanovic.spirala.DateUtil;
import edis.krsmanovic.spirala.HttpService;
import edis.krsmanovic.spirala.MainActivity;
import edis.krsmanovic.spirala.MyDBOpenHelper;

class TransactionListInteractor implements ITransactionListInteractor {

    private final ITransactionListView transactionListView;
    private Account account;
    private List<Transaction> transactionList;
    private Context context;

    public TransactionListInteractor(Account account, ITransactionListView transactionListView, Context context) {
        this.account = account;
        this.transactionList = account.getTransactionList();
        this.transactionListView = transactionListView;
        this.context = context;
    }

    @Override
    public List<Transaction> getAll() {
        return account.getTransactionList();
    }

    @Override
    public void changeTransaction(Transaction newTransaction) {
        List<Transaction> transactionList = account.getTransactionList();
        int size = transactionList.size();
        for (int i = 0; i < size; i++) {
            if (transactionList.get(i).getId().equals(newTransaction.getId())) {
                transactionList.set(i, newTransaction);
                break;
            }
        }
        addOrUpdateNewTransactionOnWebService(newTransaction);
        this.transactionList = account.getTransactionList();
    }

    @Override
    public void addNewTransaction(Transaction newTransaction) {
        account.getTransactionList().add(newTransaction);
        addOrUpdateNewTransactionOnWebService(newTransaction);
    }

    private void addOrUpdateNewTransactionOnWebService(Transaction newTransaction) {
        if (!MainActivity.hasInternetAccess) {
            //sacuvati u bazu
            ContentResolver cr = context.getApplicationContext().getContentResolver();
            Uri adresa = Uri.parse("content://rma.my.provider/transactions");

            ContentValues contentValue = new ContentValues();
            contentValue.put(MyDBOpenHelper.TRANSACTION_ID, newTransaction.getExternalId());
            contentValue.put(MyDBOpenHelper.TRANSACTION_INTERNAL_ID, newTransaction.getId());
            contentValue.put(MyDBOpenHelper.TRANSACTION_DATE, DateUtil.toString(newTransaction.getDate()));
            contentValue.put(MyDBOpenHelper.TRANSACTION_AMOUNT, newTransaction.getAmount());
            contentValue.put(MyDBOpenHelper.TRANSACTION_TITLE, newTransaction.getTitle());
            contentValue.put(MyDBOpenHelper.TRANSACTION_TYPE, newTransaction.getType().toString());
            contentValue.put(MyDBOpenHelper.TRANSACTION_ITEMDESCRIPTION, newTransaction.getItemDescription());
            contentValue.put(MyDBOpenHelper.TRANSACTION_TRANSACTIONINTERVAL, newTransaction.getTransactionInterval());
            contentValue.put(MyDBOpenHelper.TRANSACTION_ENDDATE, DateUtil.toString(newTransaction.getEndDate()));
            cr.insert(adresa, contentValue);
        } else {
            HttpService httpService = new HttpService();
            httpService.execute(null, "POST", "http://rma20-app-rmaws.apps.us-west-1.starter.openshift-online.com/account/190a54f8-b2a5-4221-b9fe-500726a50a30/transactions", newTransaction, "NEW");
        }
    }

    @Override
    public void deleteTransaction(Integer id) {
        boolean hasExternal = false;
        List<Transaction> transactionList = account.getTransactionList();
        for (Transaction transaction : transactionList) {
            if (transaction.getId().equals(id)) {
                transactionList.remove(transaction);
                if (transaction.getExternalId() != null && transaction.getExternalId() != 0) {
                    hasExternal = true;
                    id = transaction.getExternalId();
                }
                break;
            }
        }
        if (hasExternal) removeTransactionFromWebService(id);
        this.transactionList = account.getTransactionList();
    }

    private void removeTransactionFromWebService(Integer id) {
        if (MainActivity.hasInternetAccess) {
            HttpService httpService = new HttpService();
            httpService.execute(null, "DELETE", "http://rma20-app-rmaws.apps.us-west-1.starter.openshift-online.com/account/190a54f8-b2a5-4221-b9fe-500726a50a30/transactions/" + id);
        } else {
            ContentResolver cr = context.getApplicationContext().getContentResolver();
            Uri adresa = Uri.parse("content://rma.my.provider/toDelete");

            ContentValues contentValue = new ContentValues();
            contentValue.put(MyDBOpenHelper.TODELETE_ID, id);
            cr.insert(adresa, contentValue);
        }
    }

    @Override
    public Integer getNextId() {
        if (!MainActivity.hasInternetAccess) {
            int max = 0;
            for (Transaction transaction : account.getTransactionList()) {
                Integer id = transaction.getId();
                if (id > max) max = id;
            }
            return max + 1;
        }
        return null;
    }

    private List<Transaction> filterTransactionsByFilter(TransactionType filter, List<Transaction> transactionList) {
        List<Transaction> newTransactionList = new ArrayList<>();
        for (Transaction transaction : transactionList) {
            if (transaction.getType() == filter) {
                newTransactionList.add(transaction);
            }
        }
        transactionList = newTransactionList;
        return transactionList;
    }

    public List<Transaction> filterTransactionsByDate(int month, int year, List<Transaction> transactionList) {
        List<Transaction> newTransactionList = new ArrayList<>();
        for (Transaction transaction : transactionList) {
            if (!transaction.getType().toString().toLowerCase().contains("regular")) {
                if (DateUtil.getMonth(transaction.getDate()) == month && DateUtil.getYear(transaction.getDate()) == year) {
                    newTransactionList.add(transaction);
                }
            } else {
                Date date = transaction.getDate();
                while (date.before(transaction.getEndDate())) {
                    if (DateUtil.getMonth(date) == month && DateUtil.getYear(date) == year) {
                        newTransactionList.add(transaction);
                    }
                    date = DateUtil.advanceDays(date, transaction.getTransactionInterval());
                }
            }
        }
        return newTransactionList;
    }

    public List<Transaction> getAllTransactionsFromDB() {
        ContentResolver cr = context.getApplicationContext().getContentResolver();
        String[] kolone = new String[]{
                MyDBOpenHelper.TRANSACTION_ID,
                MyDBOpenHelper.TRANSACTION_INTERNAL_ID,
                MyDBOpenHelper.TRANSACTION_DATE,
                MyDBOpenHelper.TRANSACTION_AMOUNT,
                MyDBOpenHelper.TRANSACTION_TITLE,
                MyDBOpenHelper.TRANSACTION_TYPE,
                MyDBOpenHelper.TRANSACTION_ITEMDESCRIPTION,
                MyDBOpenHelper.TRANSACTION_TRANSACTIONINTERVAL,
                MyDBOpenHelper.TRANSACTION_ENDDATE
        };
        Uri adresa = Uri.parse("content://rma.my.provider/transactions");
        String where = null;
        String whereArgs[] = null;
        String order = null;
        Cursor cur = cr.query(adresa, kolone, where, whereArgs, order);
        List<Transaction> transactionList = new ArrayList<>();
        cur.moveToFirst();
        int n = cur.getCount();
        for (int i = 0; i < n; i++) {
            Transaction transaction = new Transaction(
                    cur.getInt(cur.getColumnIndex(MyDBOpenHelper.TRANSACTION_INTERNAL_ID)),
                    cur.getInt(cur.getColumnIndex(MyDBOpenHelper.TRANSACTION_ID)),
                    DateUtil.getDate(cur.getString(cur.getColumnIndex(MyDBOpenHelper.TRANSACTION_DATE))),
                    cur.getDouble(cur.getColumnIndex(MyDBOpenHelper.TRANSACTION_AMOUNT)),
                    cur.getString(cur.getColumnIndex(MyDBOpenHelper.TRANSACTION_TITLE)),
                    TransactionType.valueOf(cur.getString(cur.getColumnIndex(MyDBOpenHelper.TRANSACTION_TYPE))),
                    cur.getString(cur.getColumnIndex(MyDBOpenHelper.TRANSACTION_ITEMDESCRIPTION)),
                    cur.getInt(cur.getColumnIndex(MyDBOpenHelper.TRANSACTION_INTERNAL_ID)),
                    DateUtil.getDate(cur.getString(cur.getColumnIndex(MyDBOpenHelper.TRANSACTION_ENDDATE))));
            transactionList.add(transaction);
            cur.moveToNext();
        }
        cur.close();
        return transactionList;
    }

    public void updateWebService() {
        if (MainActivity.hasInternetAccess) {
            List<Transaction> transactionList = getAllTransactionsFromDB();

            for (Transaction transaction : transactionList) {
                addOrUpdateNewTransactionOnWebService(transaction);
            }

            List<Integer> idsToDelete = getAllTransactionIdsToDelete();
            for (Integer id : idsToDelete) {
                removeTransactionFromWebService(id);
            }



            //brisi sve iz baze

            ContentResolver cr = context.getApplicationContext().getContentResolver();
            Uri adresa = Uri.parse("content://rma.my.provider/transactions");
            cr.delete(adresa, null, null);
        }
    }


    private List<Integer> getAllTransactionIdsToDelete() {
        ContentResolver cr = context.getApplicationContext().getContentResolver();
        String[] kolone = new String[]{"id"};
        Uri adresa = Uri.parse("content://rma.my.provider/toDelete");
        String where = null;
        String whereArgs[] = null;
        String order = null;
        Cursor cur = cr.query(adresa, kolone, where, whereArgs, order);
        List<Integer> idList = new ArrayList<>();
        cur.moveToFirst();
        int n = cur.getCount();
        for (int i = 0; i < n; i++) {
            Integer id = cur.getInt(cur.getColumnIndex("id"));
            idList.add(id);
            cur.moveToNext();
        }
        cur.close();
        return idList;
    }
}
