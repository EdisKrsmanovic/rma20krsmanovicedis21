package edis.krsmanovic.spirala.TransactionList;

import java.util.List;
import java.util.Comparator;

public interface ITransactionListView {
    void setTransactions(List<Transaction> transactionList);
    int getMonth();
    int getYear();
    TransactionType getFilter();
    String getSortCriteria();
    void notifyTransactionListDataSetChanged();
}
