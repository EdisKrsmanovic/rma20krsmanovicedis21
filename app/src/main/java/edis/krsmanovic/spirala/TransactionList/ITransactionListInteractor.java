package edis.krsmanovic.spirala.TransactionList;

import java.util.List;
import java.util.Comparator;

public interface ITransactionListInteractor {
    List<Transaction> getAll();
    void changeTransaction(Transaction newTransaction);
    void deleteTransaction(Integer id);
    void addNewTransaction(Transaction newTransaction);
    Integer getNextId();
}
