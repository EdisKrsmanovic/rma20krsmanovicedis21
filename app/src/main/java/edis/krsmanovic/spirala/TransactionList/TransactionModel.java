package edis.krsmanovic.spirala.TransactionList;

import java.util.ArrayList;

import edis.krsmanovic.spirala.DateUtil;

public class TransactionModel {
    public static ArrayList<Transaction> getTransactions() {
        return new ArrayList<Transaction>() {
            {
                add(new Transaction(1,
                        DateUtil.getDate(2020, 3, 25), -100d,
                        "PayPal", TransactionType.INDIVIDUALINCOME, null,
                        null, null));
                add(new Transaction(2,
                        DateUtil.getDate(2020, 4, 21), -200d,
                        "Bank", TransactionType.REGULARINCOME, null,
                        30, DateUtil.getDate(2020, 6, 14)));
                add(new Transaction(3,
                        DateUtil.getDate(2020, 3, 20), 66.25d,
                        "Bitcoin", TransactionType.INDIVIDUALPAYMENT, "Bitcoin deposit",
                        null, null));
                add(new Transaction(4,
                        DateUtil.getDate(2020, 5, 1), 23.1d,
                        "Payoneer", TransactionType.REGULARPAYMENT, "Money spent on Payoneer",
                        5, DateUtil.getDate(2020, 7, 5)));
                add(new Transaction(5,
                        DateUtil.getDate(2020, 4, 30), 56.2d,
                        "Venmo", TransactionType.PURCHASE, "Venmo purchase",
                        null, null));
                add(new Transaction(6,
                        DateUtil.getDate(2020, 2, 15), 11.2d,
                        "Cash App", TransactionType.PURCHASE, "Cash App purchase",
                        null, null));
                add(new Transaction(7,
                        DateUtil.getDate(2020, 1, 25), -100d,
                        "PayPal", TransactionType.INDIVIDUALINCOME, null,
                        null, null));
                add(new Transaction(8,
                        DateUtil.getDate(2019, 12, 21), -2500d,
                        "Bank", TransactionType.REGULARINCOME, null,
                        30, DateUtil.getDate(2020, 2, 14)));
                add(new Transaction(9,
                        DateUtil.getDate(2020, 4, 1), 156.5d,
                        "Charity", TransactionType.INDIVIDUALPAYMENT, "Charity for animals",
                        null, null));
                add(new Transaction(10,
                        DateUtil.getDate(2019, 7, 1), 5d,
                        "Payoneer", TransactionType.REGULARPAYMENT, "Netflix subscription",
                        15, DateUtil.getDate(2020, 1, 5)));
                add(new Transaction(11,
                        DateUtil.getDate(2019, 12, 30), 15.2d,
                        "PayPal", TransactionType.PURCHASE, "PayPal transaction #762345",
                        null, null));
                add(new Transaction(12,
                        DateUtil.getDate(2020, 1, 15), 261d,
                        "Cash App", TransactionType.PURCHASE, "Cash App purchase",
                        null, null));
                add(new Transaction(13,
                        DateUtil.getDate(2020, 2, 12), -154.5d,
                        "PayPal", TransactionType.INDIVIDUALINCOME, null,
                        null, null));
                add(new Transaction(14,
                        DateUtil.getDate(2020, 2, 21), 12d,
                        "Credit card", TransactionType.INDIVIDUALPAYMENT, "McDonalds order #2345",
                        null, null));
                add(new Transaction(15,
                        DateUtil.getDate(2020, 5, 20), 66.25d,
                        "Cash", TransactionType.INDIVIDUALPAYMENT, "Starbucks order #6123",
                        null, null));
                add(new Transaction(16,
                        DateUtil.getDate(2020, 1, 1), 23.1d,
                        "Credit card", TransactionType.REGULARPAYMENT, "Gym subscription",
                        30, DateUtil.getDate(2020, 12, 5)));
                add(new Transaction(17,
                        DateUtil.getDate(2019, 9, 30), 56.2d,
                        "Venmo", TransactionType.PURCHASE, "Venmo purchase",
                        null, null));
                add(new Transaction(18,
                        DateUtil.getDate(2020, 6, 15), 11.2d,
                        "Cash App", TransactionType.PURCHASE, "Cash App purchase",
                        null, null));
                add(new Transaction(19,
                        DateUtil.getDate(2020, 1, 25), -50d,
                        "PayPal", TransactionType.INDIVIDUALINCOME, null,
                        null, null));
                add(new Transaction(20,
                        DateUtil.getDate(2019, 11, 21), -2500d,
                        "PayPal", TransactionType.INDIVIDUALINCOME, null,
                        null, null));
                add(new Transaction(21,
                        DateUtil.getDate(2020, 6, 1), 256.5d,
                        "Charity", TransactionType.INDIVIDUALPAYMENT, "Charity for homeless",
                        null, null));
                add(new Transaction(22,
                        DateUtil.getDate(2020, 8, 1), 5d,
                        "Bank", TransactionType.REGULARPAYMENT, "Youtube subscription",
                        30, DateUtil.getDate(2020, 10, 5)));
                add(new Transaction(23,
                        DateUtil.getDate(2019, 12, 30), 15.2d,
                        "PayPal", TransactionType.PURCHASE, "PayPal transaction #541246",
                        null, null));
                add(new Transaction(24,
                        DateUtil.getDate(2020, 11, 15), 261d,
                        "Cash App", TransactionType.PURCHASE, "Cash App purchase",
                        null, null));
            }
        };
    }
}
