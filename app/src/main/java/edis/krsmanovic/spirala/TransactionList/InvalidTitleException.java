package edis.krsmanovic.spirala.TransactionList;

public class InvalidTitleException extends Exception {
    public InvalidTitleException(String s) {
        super(s);
    }
}
