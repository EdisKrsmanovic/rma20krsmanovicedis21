package edis.krsmanovic.spirala;

import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.widget.FrameLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.viewpager.widget.ViewPager;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutionException;

import edis.krsmanovic.spirala.Account.Account;
import edis.krsmanovic.spirala.TransactionDetail.TransactionDetailFragment;
import edis.krsmanovic.spirala.TransactionList.Transaction;
import edis.krsmanovic.spirala.TransactionList.TransactionListFragment;
import edis.krsmanovic.spirala.TransactionList.TransactionType;

public class MainActivity extends AppCompatActivity implements TransactionListFragment.TransactionListHandler, TransactionDetailFragment.TransactionHandler {
    public static Account account = new Account(10000d, 12000d, 1000d, new ArrayList<>());
    public static boolean hasInternetAccess = false;

    private boolean twoPaneMode;
    private TransactionDetailFragment transactionDetailFragment;
    private TransactionListFragment transactionListFragment;
    private int selectedTransactionId = -1;
    private ViewPager viewPager;
    private ScreenSlidePagerAdapter pagerAdapter;

    private boolean lastInternetAccessState = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        FragmentManager fragmentManager = getSupportFragmentManager();

        FrameLayout details = findViewById(R.id.transaction_detail);

        if (details != null) {
            twoPaneMode = true;
            TransactionDetailFragment detailFragment = (TransactionDetailFragment) fragmentManager.findFragmentById(R.id.transaction_detail);
            onAddClicked(getNextId(), calculateCurrentTotalAmount(), (ArrayList<Transaction>) account.getTransactionList(), account);
            this.transactionDetailFragment = detailFragment;

            Fragment listFragment = fragmentManager.findFragmentById(R.id.transaction_list);

            if (listFragment == null || listFragment instanceof TransactionDetailFragment) {
                listFragment = new TransactionListFragment();
                fragmentManager.beginTransaction()
                        .replace(R.id.transaction_list, listFragment)
                        .commit();
            } else {
                fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            }
            this.transactionListFragment = (TransactionListFragment) listFragment;
        } else {
            twoPaneMode = false;
            viewPager = findViewById(R.id.view_pager);
            pagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager(), 0);
            viewPager.setAdapter(pagerAdapter);
            viewPager.setCurrentItem(1);
            viewPager.setOffscreenPageLimit(0);
            viewPager.addOnPageChangeListener(pageChangedListener());
            this.transactionListFragment = pagerAdapter.getTransactionListFragment();
            getTransactionsFromUrl();
            getAccountInfo();
        }
        new Timer().scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                if (MainActivity.hasInternetAccess && !lastInternetAccessState) {
                    updateWebService();
                    transactionListFragment.getPresenter().refreshAll();
                }
                if(MainActivity.hasInternetAccess != lastInternetAccessState) {
                    runOnUiThread(() -> transactionListFragment.setOfflineTextVisibility(MainActivity.hasInternetAccess));
                }
                lastInternetAccessState = MainActivity.hasInternetAccess;

                checkInternetAccess();
            }
        }, 0, 5000);//put here time 1000 milliseconds=1 second
    }

    private void updateWebService() {
        transactionListFragment.getPresenter().updateWebService();
    }

    public static void checkInternetAccess() {
        try {
            HttpGet httpGet = new HttpGet("http://yahoo.com");
            HttpParams httpParameters = new BasicHttpParams();
            int timeoutConnection = 30000;
            HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
            int timeoutSocket = 40000;
            HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);

            DefaultHttpClient httpClient = new DefaultHttpClient(httpParameters);
            HttpResponse response = httpClient.execute(httpGet);
            MainActivity.hasInternetAccess = true;
        } catch (Exception e) {
            MainActivity.hasInternetAccess = false;
        }
    }

    private void getAccountInfo() {
        try {
            String url = "http://rma20-app-rmaws.apps.us-west-1.starter.openshift-online.com/account/190a54f8-b2a5-4221-b9fe-500726a50a30";
            HttpService httpService = new HttpService();
            StringBuilder response = httpService.execute(pagerAdapter, "GET", url).get();
            JSONObject jo = new JSONObject(String.valueOf(response));
            Double budget = jo.getDouble("budget");
            Double totalLimit = jo.getDouble("totalLimit");
            Double monthLimit = jo.getDouble("monthLimit");
            account.setBudget(budget);
            account.setTotalLimit(totalLimit);
            account.setMonthLimit(monthLimit);
            hasInternetAccess = true;
            lastInternetAccessState = true;
        } catch (InterruptedException | ExecutionException | JSONException e) {
            e.printStackTrace();
            hasInternetAccess = false;
        }
    }

    private void getTransactionsFromUrl() {
        new Thread(() -> {
            try {
                int page = 0;
                while (true) {
                    String url = "http://rma20-app-rmaws.apps.us-west-1.starter.openshift-online.com/account/190a54f8-b2a5-4221-b9fe-500726a50a30/transactions?page=" + page;
                    page++;
                    HttpService httpService = new HttpService();
                    StringBuilder response = httpService.execute(pagerAdapter, "GET", url).get();
                    JSONObject jo = new JSONObject(String.valueOf(response));
                    JSONArray results = jo.getJSONArray("transactions");
                    if (results.length() == 0) break;
                    for (int i = 0; i < results.length(); i++) {

                        JSONObject transactionObject = results.getJSONObject(i);
                        Integer id = transactionObject.getInt("id");

                        String dateString = transactionObject.getString("date");
                        Date date = DateUtil.getDate(
                                Integer.parseInt(dateString.substring(0, 4)),
                                Integer.parseInt(dateString.substring(5, 7)),
                                Integer.parseInt(dateString.substring(8, 10)));

                        String title = transactionObject.getString("title");

                        Double amount = transactionObject.getDouble("amount");

                        String itemDescription = transactionObject.getString("itemDescription");

                        Integer transactionInterval = null;
                        try {
                            transactionInterval = transactionObject.getInt("transactionInterval");
                        } catch (JSONException ignored) {
                        }

                        String endDateString = transactionObject.getString("endDate");
                        Date endDate = null;
                        if (!endDateString.equals("null")) {
                            endDate = DateUtil.getDate(
                                    Integer.parseInt(endDateString.substring(0, 4)),
                                    Integer.parseInt(endDateString.substring(5, 7)),
                                    Integer.parseInt(endDateString.substring(8, 10)));
                        }
                        Integer transactionTypeId = transactionObject.getInt("TransactionTypeId");

                        TransactionType[] transactionTypes = {TransactionType.REGULARPAYMENT, TransactionType.REGULARINCOME, TransactionType.PURCHASE, TransactionType.INDIVIDUALINCOME, TransactionType.INDIVIDUALPAYMENT};

                        TransactionType transactionType = transactionTypes[transactionTypeId - 1];

                        Transaction transaction = new Transaction(id, date, amount, title, transactionType, itemDescription, transactionInterval, endDate);

                        account.getTransactionList().add(transaction);
                    }
                }
                hasInternetAccess = true;
            } catch (InterruptedException | ExecutionException | JSONException e) {
                hasInternetAccess = false;
                e.printStackTrace();
            }
        }).start();
    }

    private ViewPager.OnPageChangeListener pageChangedListener() {
        return new ViewPager.OnPageChangeListener() {
            private int pagerPosition;

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                this.pagerPosition = position;
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                if (state == 0) {
                    if (pagerPosition == 0) {
                        ((ViewPager) viewPager).setCurrentItem(3, false);
                    } else if (pagerPosition == 4) {
                        ((ViewPager) viewPager).setCurrentItem(1, false);
                    }
                }
            }
        };
    }

    public Integer getNextId() {
        if (!hasInternetAccess) {
            int max = 0;
            for (Transaction transaction : account.getTransactionList()) {
                Integer id = transaction.getId();
                if (id > max) max = id;
            }
            return max + 1;
        }
        return null;
    }


    private Double calculateCurrentTotalAmount() {
        Double totalAmount = 0d;
        for (Transaction transaction : account.getTransactionList()) {
            if (!transaction.getType().toString().toLowerCase().contains("regular")) {
                totalAmount += transaction.getAmount();
            } else {
                Date startDate = transaction.getDate();
                Date endDate = transaction.getEndDate();
                while (startDate.before(endDate)) {
                    totalAmount += transaction.getAmount();
                    startDate = DateUtil.advanceDays(startDate, transaction.getTransactionInterval());
                }
            }
        }
        return totalAmount;
    }


    @Override
    public void onItemClicked(Transaction transaction, Double currentTotal, ArrayList<Transaction> allTransactions, Account account) {
        Bundle arguments = new Bundle();
        if (selectedTransactionId == transaction.getId() && twoPaneMode) {
            selectedTransactionId = -1;
            onAddClicked(getNextId(), calculateCurrentTotalAmount(), (ArrayList<Transaction>) account.getTransactionList(), account);
            return;
        }

        selectedTransactionId = transaction.getId();
        arguments.putSerializable("TRANSACTION", transaction);
        arguments.putDouble("TOTALLIMIT", account.getTotalLimit());
        arguments.putDouble("MONTHLIMIT", account.getMonthLimit());
        arguments.putSerializable("CURRENTTOTALAMOUNT", currentTotal);
        arguments.putSerializable("ALLTRANSACTIONS", allTransactions);
        arguments.putSerializable("ACTION", "EDIT");
        if (!twoPaneMode) {
            pagerAdapter.setArguments(arguments);
            pagerAdapter.setFragmentType(1); //0 list, 1 detail
            pagerAdapter.notifyDataSetChanged();
            this.transactionDetailFragment = pagerAdapter.getTransactionDetailFragment();
        } else {
            this.transactionDetailFragment = new TransactionDetailFragment();
            this.transactionDetailFragment.setArguments(arguments);
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.transaction_detail, transactionDetailFragment)
                    .addToBackStack(null)
                    .commit();
        }
    }

    @Override
    public void onAddClicked(Integer id, Double currentTotal, ArrayList<Transaction> allTransactions, Account account) {
        Bundle arguments = new Bundle();
        arguments.putSerializable("TRANSACTION", new Transaction(id));
        arguments.putDouble("TOTALLIMIT", account.getTotalLimit());
        arguments.putDouble("MONTHLIMIT", account.getMonthLimit());
        arguments.putSerializable("CURRENTTOTALAMOUNT", currentTotal);
        arguments.putSerializable("ALLTRANSACTIONS", allTransactions);
        arguments.putSerializable("ACTION", "ADDNEW");
        transactionDetailFragment = new TransactionDetailFragment();
        transactionDetailFragment.setArguments(arguments);
        if (!twoPaneMode) {
            pagerAdapter.setArguments(arguments);
            pagerAdapter.setFragmentType(1); //0 list, 1 detail
            pagerAdapter.notifyDataSetChanged();
            this.transactionDetailFragment = pagerAdapter.getTransactionDetailFragment();
        } else {
            this.transactionDetailFragment = new TransactionDetailFragment();
            this.transactionDetailFragment.setArguments(arguments);
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.transaction_detail, transactionDetailFragment)
                    .commit();
        }
    }

    @Override
    public void saveTransaction(Transaction transaction) {
        transactionListFragment.saveTransaction(transaction);
        if (!twoPaneMode) {
            pagerAdapter.setFragmentType(0); //0 list, 1 detail
            pagerAdapter.notifyDataSetChanged();
            pagerAdapter.reloadGraphValues();
        }
    }

    @Override
    public void addNewTransaction(Transaction newTransaction) {
        transactionListFragment.addNewTransaction(newTransaction);
        if (!twoPaneMode) {
            pagerAdapter.setFragmentType(0); //0 list, 1 detail
            pagerAdapter.notifyDataSetChanged();
            pagerAdapter.reloadGraphValues();
        }
    }

    @Override
    public void deleteTransaction(Transaction transaction) {
        transactionListFragment.deleteTransaction(transaction);
        if (!twoPaneMode) {
            pagerAdapter.setFragmentType(0); //0 list, 1 detail
            pagerAdapter.notifyDataSetChanged();
            pagerAdapter.reloadGraphValues();
        }
    }

    @Override
    public void cancelAction() {
        if (!twoPaneMode) {
            pagerAdapter.setFragmentType(0); //0 list, 1 detail
            pagerAdapter.notifyDataSetChanged();
        }
    }
}
