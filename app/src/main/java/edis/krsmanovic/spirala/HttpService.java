package edis.krsmanovic.spirala;

import android.os.AsyncTask;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import edis.krsmanovic.spirala.Account.AccountEntity;
import edis.krsmanovic.spirala.TransactionList.Transaction;
import edis.krsmanovic.spirala.TransactionList.TransactionEntity;
import edis.krsmanovic.spirala.TransactionList.TransactionListPresenter;
import edis.krsmanovic.spirala.TransactionList.TransactionType;

public class HttpService extends AsyncTask<Object, Void, StringBuilder> {

    private Object dataSet;
    private List<Transaction> transactionList;

    public HttpService() {
    }

    // 0 - dataset
    // 1 - method
    // 2 - url
    @Override
    protected StringBuilder doInBackground(Object... objects) {
        MainActivity.checkInternetAccess();
        if(!MainActivity.hasInternetAccess) return null;
        this.dataSet = objects[0];
        if (this.dataSet instanceof TransactionListPresenter) {
            setFilteredTransactions();
            return null;
        } else if (objects[1].toString().equals("POST")) {
            if (objects[2].toString().contains("transactions")) {
                Transaction transaction = (Transaction) objects[3];
                try {
                    String urlString = objects[2].toString();
                    if (transaction.getExternalId() != null && transaction.getExternalId() != 0) {
                        urlString += "/" + transaction.getExternalId();
                    }
                    URL url = new URL(urlString);
                    HttpURLConnection con = (HttpURLConnection) url.openConnection();
                    con.setRequestMethod("POST");
                    con.setRequestProperty("Content-Type", "application/json");
                    con.setRequestProperty("Accept", "*/*");
                    con.setDoOutput(true);

                    TransactionEntity transactionEntity = new TransactionEntity(transaction);
                    Gson gson = new Gson();
                    String jsonInputString = gson.toJson(transactionEntity);
                    try (OutputStream os = con.getOutputStream()) {
                        byte[] input = jsonInputString.getBytes("utf-8");
                        os.write(input, 0, input.length);
                    }
                    try (BufferedReader br = new BufferedReader(
                            new InputStreamReader(con.getInputStream(), "utf-8"))) {
                        StringBuilder response = new StringBuilder();
                        String responseLine = null;
                        while ((responseLine = br.readLine()) != null) {
                            response.append(responseLine.trim());
                        }
                        if (transaction.getExternalId() == null) {
                            JSONObject jo = new JSONObject(String.valueOf(response));
                            int id = jo.getInt("id");
                            transaction.setId(id);
                            transaction.setExternalId(id);
                        }
                    }
                    MainActivity.hasInternetAccess = true;
                } catch (IOException | JSONException e) {
                    MainActivity.hasInternetAccess = false;
                    e.printStackTrace();
                }
            } else if (objects[2].toString().contains("account")) {
                try {
                    URL url = new URL(objects[2].toString());
                    HttpURLConnection con = (HttpURLConnection) url.openConnection();
                    con.setRequestMethod("POST");
                    con.setRequestProperty("Content-Type", "application/json");
                    con.setRequestProperty("Accept", "*/*");
                    con.setDoOutput(true);

                    AccountEntity accountEntity = new AccountEntity(MainActivity.account);
                    Gson gson = new Gson();
                    String jsonInputString = gson.toJson(accountEntity);
                    try (OutputStream os = con.getOutputStream()) {
                        byte[] input = jsonInputString.getBytes("utf-8");
                        os.write(input, 0, input.length);
                    }
                    try (BufferedReader br = new BufferedReader(
                            new InputStreamReader(con.getInputStream(), "utf-8"))) {
                        StringBuilder response = new StringBuilder();
                        String responseLine = null;
                        while ((responseLine = br.readLine()) != null) {
                            response.append(responseLine.trim());
                        }
                    }
                    MainActivity.hasInternetAccess = true;
                } catch (IOException e) {
                    MainActivity.hasInternetAccess = false;
//                    e.printStackTrace();
                }
            }
            return null;
        } else if (objects[1].toString().equals("DELETE")) {
            try {
                URL url = new URL(objects[2].toString());
                HttpURLConnection con = (HttpURLConnection) url.openConnection();
                con.setRequestMethod("DELETE");
                con.setRequestProperty("Content-Type", "application/json");
                con.setRequestProperty("Accept", "*/*");
                try (BufferedReader br = new BufferedReader(
                        new InputStreamReader(con.getInputStream(), "utf-8"))) {
                    StringBuilder response = new StringBuilder();
                    String responseLine = null;
                    while ((responseLine = br.readLine()) != null) {
                        response.append(responseLine.trim());
                    }
                }
                MainActivity.hasInternetAccess = true;
            } catch (IOException e) {
                MainActivity.hasInternetAccess = false;
//                e.printStackTrace();
            }
            return null;
        } else {
            try {

                if (this.dataSet instanceof ScreenSlidePagerAdapter) {
                    setAllTransactions();
                }
                URL url = new URL(objects[2].toString());
                HttpURLConnection con = (HttpURLConnection) url.openConnection();
                con.setRequestMethod(objects[1].toString());
                InputStream in = new BufferedInputStream(con.getInputStream());
                try (BufferedReader br = new BufferedReader(
                        new InputStreamReader(con.getInputStream(), "utf-8"))) {
                    StringBuilder response = new StringBuilder();
                    String responseLine;
                    while ((responseLine = br.readLine()) != null) {
                        response.append(responseLine.trim());
                    }
                    MainActivity.hasInternetAccess = true;
                    return response;
                }
            } catch (Exception e) {
                MainActivity.hasInternetAccess = false;
//                e.printStackTrace();
            }
            return null;
        }
    }

    private void setAllTransactions() {
        this.transactionList = new ArrayList<>();

        try {
            int page = 0;
            while (true) {
                List<TransactionType> transactionTypeList = new ArrayList<>();
                transactionTypeList.add(TransactionType.REGULARPAYMENT);
                transactionTypeList.add(TransactionType.REGULARINCOME);
                transactionTypeList.add(TransactionType.PURCHASE);
                transactionTypeList.add(TransactionType.INDIVIDUALINCOME);
                transactionTypeList.add(TransactionType.INDIVIDUALPAYMENT);


                String urlString = "http://rma20-app-rmaws.apps.us-west-1.starter.openshift-online.com/account/190a54f8-b2a5-4221-b9fe-500726a50a30/transactions/filter?page=" + page;
                page++;
                URL url = new URL(urlString);
                HttpURLConnection con = (HttpURLConnection) url.openConnection();
                con.setRequestMethod("GET");
                StringBuilder response = new StringBuilder();
                try (BufferedReader br = new BufferedReader(
                        new InputStreamReader(con.getInputStream(), "utf-8"))) {
                    String responseLine;
                    while ((responseLine = br.readLine()) != null) {
                        response.append(responseLine.trim());
                    }
                }
                if (response.length() == 0) break;
                JSONObject jo = new JSONObject(String.valueOf(response));
                JSONArray results = jo.getJSONArray("transactions");
                if (results.length() == 0) break;
                for (int i = 0; i < results.length(); i++) {

                    JSONObject transactionObject = results.getJSONObject(i);
                    Integer id = transactionObject.getInt("id");

                    String dateString = transactionObject.getString("date");
                    Date date = DateUtil.getDate(
                            Integer.parseInt(dateString.substring(0, 4)),
                            Integer.parseInt(dateString.substring(5, 7)),
                            Integer.parseInt(dateString.substring(8, 10)));

                    String title = transactionObject.getString("title");

                    Double amount = transactionObject.getDouble("amount");

                    String itemDescription = transactionObject.getString("itemDescription");

                    Integer transactionInterval = null;
                    try {
                        transactionInterval = transactionObject.getInt("transactionInterval");
                    } catch (JSONException ignored) {
                    }

                    String endDateString = transactionObject.getString("endDate");
                    Date endDate = null;
                    if (!endDateString.equals("null")) {
                        endDate = DateUtil.getDate(
                                Integer.parseInt(endDateString.substring(0, 4)),
                                Integer.parseInt(endDateString.substring(5, 7)),
                                Integer.parseInt(endDateString.substring(8, 10)));
                    }
                    Integer transactionTypeId = transactionObject.getInt("TransactionTypeId");

                    TransactionType[] transactionTypes = {TransactionType.REGULARPAYMENT, TransactionType.REGULARINCOME, TransactionType.PURCHASE, TransactionType.INDIVIDUALINCOME, TransactionType.INDIVIDUALPAYMENT};

                    TransactionType transactionType = transactionTypes[transactionTypeId - 1];

                    Transaction transaction = new Transaction(id, id, date, amount, title, transactionType, itemDescription, transactionInterval, endDate);


                    if (!transaction.getType().toString().toLowerCase().contains("regular")) {
                        transactionList.add(transaction);
                    } else {
                        Date transactionDate = transaction.getDate();
                        while (transactionDate.before(transaction.getEndDate())) {
                            transactionList.add(transaction);
                            transactionDate = DateUtil.advanceDays(transactionDate, transaction.getTransactionInterval());
                        }
                    }
                }
            }
            MainActivity.hasInternetAccess = true;
        } catch (JSONException | IOException e) {
            MainActivity.hasInternetAccess = false;
//            e.printStackTrace();
        }
    }

    private void setFilteredTransactions() {
        this.transactionList = new ArrayList<>();
        TransactionListPresenter transactionListPresenter = (TransactionListPresenter) this.dataSet;

        try {
            int page = 0;
            while (true) {
                List<TransactionType> transactionTypeList = new ArrayList<>();
                transactionTypeList.add(TransactionType.REGULARPAYMENT);
                transactionTypeList.add(TransactionType.REGULARINCOME);
                transactionTypeList.add(TransactionType.PURCHASE);
                transactionTypeList.add(TransactionType.INDIVIDUALINCOME);
                transactionTypeList.add(TransactionType.INDIVIDUALPAYMENT);

                Integer typeId = transactionTypeList.indexOf(transactionListPresenter.getTransactionListView().getFilter());
                if (typeId != -1) typeId++;

                String urlString = "http://rma20-app-rmaws.apps.us-west-1.starter.openshift-online.com/account/190a54f8-b2a5-4221-b9fe-500726a50a30/transactions/filter?page=" + page;
                page++;
                if (typeId != -1) {
                    urlString += "&type=" + typeId;
                }
                if (transactionListPresenter.getTransactionListView().getSortCriteria() != null) {
                    urlString += "&sort=" + transactionListPresenter.getTransactionListView().getSortCriteria();
                }
                int month = transactionListPresenter.getTransactionListView().getMonth();
                int year = transactionListPresenter.getTransactionListView().getYear();
                String monthUrl;
                if (month <= 9)
                    monthUrl = "0" + month;
                else
                    monthUrl = String.valueOf(month);
                urlString += "&month=" + monthUrl + "&year=" + year;
                URL url = new URL(urlString);
                HttpURLConnection con = (HttpURLConnection) url.openConnection();
                con.setRequestMethod("GET");
                StringBuilder response = new StringBuilder();
                try (BufferedReader br = new BufferedReader(
                        new InputStreamReader(con.getInputStream(), "utf-8"))) {
                    String responseLine;
                    while ((responseLine = br.readLine()) != null) {
                        response.append(responseLine.trim());
                    }
                }
                if (response.length() == 0) break;
                JSONObject jo = new JSONObject(String.valueOf(response));
                JSONArray results = jo.getJSONArray("transactions");
                if (results.length() == 0) break;
                for (int i = 0; i < results.length(); i++) {

                    JSONObject transactionObject = results.getJSONObject(i);
                    Integer id = transactionObject.getInt("id");

                    String dateString = transactionObject.getString("date");
                    Date date = DateUtil.getDate(
                            Integer.parseInt(dateString.substring(0, 4)),
                            Integer.parseInt(dateString.substring(5, 7)),
                            Integer.parseInt(dateString.substring(8, 10)));

                    String title = transactionObject.getString("title");

                    Double amount = transactionObject.getDouble("amount");

                    String itemDescription = transactionObject.getString("itemDescription");

                    Integer transactionInterval = null;
                    try {
                        transactionInterval = transactionObject.getInt("transactionInterval");
                    } catch (JSONException ignored) {
                    }

                    String endDateString = transactionObject.getString("endDate");
                    Date endDate = null;
                    if (!endDateString.equals("null")) {
                        endDate = DateUtil.getDate(
                                Integer.parseInt(endDateString.substring(0, 4)),
                                Integer.parseInt(endDateString.substring(5, 7)),
                                Integer.parseInt(endDateString.substring(8, 10)));
                    }
                    Integer transactionTypeId = transactionObject.getInt("TransactionTypeId");

                    TransactionType[] transactionTypes = {TransactionType.REGULARPAYMENT, TransactionType.REGULARINCOME, TransactionType.PURCHASE, TransactionType.INDIVIDUALINCOME, TransactionType.INDIVIDUALPAYMENT};

                    TransactionType transactionType = transactionTypes[transactionTypeId - 1];

                    Transaction transaction = new Transaction(id, id, date, amount, title, transactionType, itemDescription, transactionInterval, endDate);


                    if (!transaction.getType().toString().toLowerCase().contains("regular")) {
                        if (DateUtil.getMonth(transaction.getDate()) == month && DateUtil.getYear(transaction.getDate()) == year) {
                            transactionList.add(transaction);
                        }
                    } else {
                        Date transactionDate = transaction.getDate();
                        while (transactionDate.before(transaction.getEndDate())) {
                            if (DateUtil.getMonth(transactionDate) == month && DateUtil.getYear(transactionDate) == year) {
                                transactionList.add(transaction);
                            }
                            transactionDate = DateUtil.advanceDays(transactionDate, transaction.getTransactionInterval());
                        }
                    }
                }
            }
            MainActivity.hasInternetAccess = true;
        } catch (JSONException | IOException e) {
            MainActivity.hasInternetAccess = false;
//            e.printStackTrace();
        }
    }

    @Override
    protected void onPostExecute(StringBuilder stringBuilder) {
        super.onPostExecute(stringBuilder);
        if (this.dataSet instanceof ScreenSlidePagerAdapter) {
            ScreenSlidePagerAdapter pagerAdapter = (ScreenSlidePagerAdapter) dataSet;
            pagerAdapter.notifyDataSetChanged();
            pagerAdapter.reloadGraphValues();
            if(transactionList.size() > 0) {
                MainActivity.account.setTransactionList(transactionList);
            }
        } else if (this.dataSet instanceof TransactionListPresenter) {
            ((TransactionListPresenter) this.dataSet).setTransactions(transactionList);

        }
    }

}
