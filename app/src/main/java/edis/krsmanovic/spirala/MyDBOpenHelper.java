package edis.krsmanovic.spirala;

import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class MyDBOpenHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "RMADataBase.db";
    public static final int DATABASE_VERSION = 1;

    public MyDBOpenHelper(@Nullable Context context, @Nullable String name, @Nullable SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    public MyDBOpenHelper(@Nullable Context context, @Nullable String name, @Nullable SQLiteDatabase.CursorFactory factory, int version, @Nullable DatabaseErrorHandler errorHandler) {
        super(context, name, factory, version, errorHandler);
    }

    public MyDBOpenHelper(@Nullable Context context, @Nullable String name, int version, @NonNull SQLiteDatabase.OpenParams openParams) {
        super(context, name, version, openParams);
    }

    public MyDBOpenHelper(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public static final String TRANSACTION_TABLE = "transactions";
    public static final String TRANSACTION_ID = "id";
    public static final String TRANSACTION_INTERNAL_ID = "internalId";
    public static final String TRANSACTION_DATE = "date";
    public static final String TRANSACTION_AMOUNT = "amount";
    public static final String TRANSACTION_TITLE = "title";
    public static final String TRANSACTION_TYPE = "type";
    public static final String TRANSACTION_ITEMDESCRIPTION = "itemDescription";
    public static final String TRANSACTION_TRANSACTIONINTERVAL = "transactionInterval";
    public static final String TRANSACTION_ENDDATE = "endDate";
    private static final String TRANSACTION_TABLE_CREATE =
            "CREATE TABLE IF NOT EXISTS " + TRANSACTION_TABLE + " (" + TRANSACTION_INTERNAL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + TRANSACTION_ID + " INTEGER UNIQUE, "
                    + TRANSACTION_DATE + " TEXT, "
                    + TRANSACTION_AMOUNT + " FLOAT(53), "
                    + TRANSACTION_TITLE + " TEXT, "
                    + TRANSACTION_TYPE + " TEXT, "
                    + TRANSACTION_ITEMDESCRIPTION + " TEXT, "
                    + TRANSACTION_TRANSACTIONINTERVAL + " INTEGER, "
                    + TRANSACTION_ENDDATE + ");";

    private static final String TRANSACTION_TABLE_DROP = "DROP TABLE IF EXISTS " + TRANSACTION_TABLE;

    public static final String ACCOUNT_TABLE = "account";
    public static final String ACCOUNT_ID = "id";
    public static final String ACCOUNT_BUDGET = "budget";
    public static final String ACCOUNT_TOTALLIMIT = "totalLimit";
    public static final String ACCOUNT_MONTHLIMIT = "monthLimit";


    private static final String ACCOUNT_TABLE_CREATE =
            "CREATE TABLE IF NOT EXISTS " + ACCOUNT_TABLE + " (" + ACCOUNT_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + ACCOUNT_BUDGET + " FLOAT(53), "
                    + ACCOUNT_TOTALLIMIT + " FLOAT(53), "
                    + ACCOUNT_MONTHLIMIT + " FLOAT(53));";


    private static final String ACCOUNT_TABLE_DROP = "DROP TABLE IF EXISTS " + ACCOUNT_TABLE;

    public static final String TODELETE_TABLE = "todelete";
    public static final String TODELETE_ID = "id";

    private static final String TODELETE_TABLE_CREATE =
            "CREATE TABLE IF NOT EXISTS " + TODELETE_TABLE + " (" + TODELETE_ID + " INTEGER);";

    private static final String TODELETE_TABLE_DROP = "DROP TABLE IF EXISTS " + TODELETE_TABLE;


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(TRANSACTION_TABLE_CREATE);
        db.execSQL(ACCOUNT_TABLE_CREATE);
        db.execSQL(TODELETE_TABLE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(TRANSACTION_TABLE_DROP);
        db.execSQL(ACCOUNT_TABLE_DROP);
        db.execSQL(TODELETE_TABLE_DROP);
        onCreate(db);
    }
}
