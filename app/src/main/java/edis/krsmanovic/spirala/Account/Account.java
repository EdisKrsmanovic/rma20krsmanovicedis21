package edis.krsmanovic.spirala.Account;

import java.io.Serializable;
import java.util.List;

import edis.krsmanovic.spirala.TransactionList.Transaction;

public class Account implements Serializable {
    private Double budget;
    private Double totalLimit;
    private Double monthLimit;
    private List<Transaction> transactionList;

    public Account() {
    }

    public Account(Double budget, Double totalLimit, Double monthLimit, List<Transaction> transactionList) {
        this.budget = budget;
        this.totalLimit = totalLimit;
        this.monthLimit = monthLimit;
        this.transactionList = transactionList;
    }

    public Double getBudget() {
        return budget;
    }

    public void setBudget(Double budget) {
        this.budget = budget;
    }

    public Double getTotalLimit() {
        return totalLimit;
    }

    public void setTotalLimit(Double totalLimit) {
        this.totalLimit = totalLimit;
    }

    public Double getMonthLimit() {
        return monthLimit;
    }

    public void setMonthLimit(Double monthLimit) {
        this.monthLimit = monthLimit;
    }

    public List<Transaction> getTransactionList() {
        return transactionList;
    }

    public void setTransactionList(List<Transaction> transactionList) {
        this.transactionList = transactionList;
    }
}
