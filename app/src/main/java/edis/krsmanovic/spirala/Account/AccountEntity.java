package edis.krsmanovic.spirala.Account;

public class AccountEntity {
    private Double budget;
    private Double totalLimit;
    private Double monthLimit;

    public AccountEntity(Account account) {
        budget = account.getBudget();
        totalLimit = account.getTotalLimit();
        monthLimit = account.getMonthLimit();
    }

    public Double getBudget() {
        return budget;
    }

    public void setBudget(Double budget) {
        this.budget = budget;
    }

    public Double getTotalLimit() {
        return totalLimit;
    }

    public void setTotalLimit(Double totalLimit) {
        this.totalLimit = totalLimit;
    }

    public Double getMonthLimit() {
        return monthLimit;
    }

    public void setMonthLimit(Double monthLimit) {
        this.monthLimit = monthLimit;
    }
}
