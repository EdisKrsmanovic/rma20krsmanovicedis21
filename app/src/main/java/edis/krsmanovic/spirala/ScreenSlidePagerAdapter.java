package edis.krsmanovic.spirala;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import edis.krsmanovic.spirala.Budget.BudgetFragment;
import edis.krsmanovic.spirala.Graphs.GraphsFragment;
import edis.krsmanovic.spirala.TransactionDetail.TransactionDetailFragment;
import edis.krsmanovic.spirala.TransactionList.TransactionListFragment;


public class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {

    private GraphsFragment graphsFragment;
    private int type;
    private Bundle arguments;
    private TransactionDetailFragment transactionDetailFragment;
    private TransactionListFragment transactionListFragment;

    public ScreenSlidePagerAdapter(@NonNull FragmentManager fm, int behavior) {
        super(fm, behavior);
        this.transactionDetailFragment = new TransactionDetailFragment();
        this.transactionListFragment = new TransactionListFragment();
        this.graphsFragment = new GraphsFragment();
        this.graphsFragment.calculateAmounts();
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        if (position == 0) {
            return new GraphsFragment();
        } else if (position == 1) {
            if (type == 0) {
//                if(transactionListFragment.getCurrentTransactions() == null) transactionListFragment.refreshAll();
                return transactionListFragment;
            } else if (type == 1) {
                TransactionDetailFragment transactionDetailFragment = new TransactionDetailFragment();
                transactionDetailFragment.setArguments(arguments);
                this.transactionDetailFragment = transactionDetailFragment;
                return transactionDetailFragment;
            }
        } else if (position == 2) {
            return new BudgetFragment(this);
        } else if (position == 3) {
            return this.graphsFragment;
        } else if (position == 4) {
            if (type == 0) {
                return new TransactionListFragment();
            } else {
                TransactionDetailFragment transactionDetailFragment = new TransactionDetailFragment();
                transactionDetailFragment.setArguments(arguments);
                return transactionDetailFragment;
            }
        }
        return this.transactionListFragment;
    }

    public void reloadGraphValues() {
        this.graphsFragment.calculateAmounts();
    }

    @Override
    public int getCount() {
        return 5;
    }

    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    public void setFragmentType(int type) {
        this.type = type;
    }

    public void setArguments(Bundle arguments) {
        this.arguments = arguments;
    }

    public TransactionDetailFragment getTransactionDetailFragment() {
        return transactionDetailFragment;
    }

    public TransactionListFragment getTransactionListFragment() {
        return transactionListFragment;
    }
}
